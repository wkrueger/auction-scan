CREATE TABLE `auc_data_raw` (
	`batch` INT(10) UNSIGNED NOT NULL,
	`faction` SMALLINT(6) NULL DEFAULT NULL,
	`auc` BIGINT(20) NOT NULL,
	`item` INT(11) NOT NULL,
	`owner` VARCHAR(50) NOT NULL,
	`ownerRealm` VARCHAR(50) NOT NULL,
	`realmGroup` VARCHAR(50) NULL DEFAULT NULL,
	`bid` BIGINT(20) NOT NULL,
	`buyout` BIGINT(20) NOT NULL,
	`quantity` INT(10) NOT NULL,
	`timeLeft` VARCHAR(15) NOT NULL,
	PRIMARY KEY (`auc`),
	INDEX `FK_auc_data_raw_db_ItemSparse_25632` (`item`),
	INDEX `FK_auc_data_raw_realm` (`ownerRealm`),
	INDEX `batch` (`batch`, `faction`),
	CONSTRAINT `FK_auc_data_raw_auc_batch` FOREIGN KEY (`batch`) REFERENCES `auc_batch` (`id`) ON DELETE CASCADE,
	CONSTRAINT `FK_auc_data_raw_db_ItemSparse_25632` FOREIGN KEY (`item`) REFERENCES `db_ItemSparse_25632` (`ID`),
	CONSTRAINT `FK_auc_data_raw_realm` FOREIGN KEY (`ownerRealm`) REFERENCES `realm` (`name`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;
