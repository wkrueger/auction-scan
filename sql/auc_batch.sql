CREATE TABLE `auc_batch` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`uuid` CHAR(32) NOT NULL,
	`lastChanged` BIGINT(20) NOT NULL,
	`realms` VARCHAR(255) NOT NULL,
	`enhanced` SMALLINT(6) NOT NULL DEFAULT '0',
	PRIMARY KEY (`id`),
	UNIQUE INDEX `Index 1` (`uuid`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;
