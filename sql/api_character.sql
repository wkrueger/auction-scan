CREATE TABLE `api_character` (
	`id` BIGINT(20) NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(25) NOT NULL,
	`realmName` VARCHAR(50) NOT NULL,
	`faction` SMALLINT(6) NOT NULL DEFAULT '-1',
	PRIMARY KEY (`id`),
	UNIQUE INDEX `name_realmName` (`name`, `realmName`),
	INDEX `FK_api_character_realm` (`realmName`),
	CONSTRAINT `FK_api_character_realm` FOREIGN KEY (`realmName`) REFERENCES `realm` (`name`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;
