import path = require("path")
import WebsiteModule from "./website"
import UserModule from "./website/user"
import KnexModule from "./common/knex"
import * as contextMappers from "./common/context-mappers"
import ScannerModule from "./common/rulescanner"
import { ContextFactory, defaultMappers, UserAuth } from "@proerd/nextpress"
//
;(async () => {
  try {
    const context = ContextFactory({
      mappers: [
        defaultMappers.website,
        defaultMappers.mailgun,
        defaultMappers.database,
        contextMappers.battleNet,
        contextMappers.webpush,
      ],
      projectRoot: path.resolve(__dirname, ".."),
    })
    const auth = new UserAuth(context)
    await KnexModule(context)
    const user = UserModule(context)
    await auth.init()
    const scanner = ScannerModule(context, user)
    await WebsiteModule(context, user, scanner)
  } catch (err) {
    console.error("top level rejection", err)
    process.exit(1)
  }
})()

declare global {
  export interface User {
    id: number
    email: string
    auth?: string
    validationHash?: string
  }
}

process.on("unhandledRejection", (...arg: any[]) => {
  console.error("unhandledRejection", ...arg)
  process.exit(1)
})
