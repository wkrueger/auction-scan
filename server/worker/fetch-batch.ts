import request = require("request")
import ono = require("ono")

export default function(
  ctx: Nextpress.Context,
  bnet: Modules.BNet,
  _getSlug: (s: string) => string,
) {
  async function run() {
    //data update
    let allSlugs: string = (await ctx.database
      .db()
      .table("realm_sub")
      .distinct("realmSlug")
      .select()).map((r: any) => r.realmSlug)
    for (let x = 0; x < allSlugs.length; x++) {
      const slug = allSlugs[x]
      try {
        await ctx.database.db().transaction(async trx => {
          let status = await fetchRealmBatch(slug, trx)
          console.log("Finished batch", status)
          if (!status.enhanced) {
            await enhanceBatchFaction(status.batch, trx)
          }
        })
        console.log("Finished on", slug)
      } catch (err) {
        console.error(new Date().toISOString(), "Error while fetching", slug)
        console.error(err)
      }
    }
  }

  /**
   * updates AH info
   */
  async function fetchRealmBatch(slug: string, trx: import("knex").Transaction) {
    console.log("fetchRealmBatch", slug)
    if (!slug) throw Error("Empty slug.")
    let check = await trx
      .table("realm")
      .select("slug")
      .where("connected", "=", slug)
    if (!check.length) throw Error("fetchRealm: invalid slug.")
    let apiFiles = await _apiRequest<AHLinkResponse>(`/wow/auction/data/${slug}`)

    if (apiFiles.files.length > 1) {
      throw Error("Unexpected more than 1 auc file (not handled).")
    }
    let uuid = apiFiles.files[0].url.replace(/.*?\/auction-data\/(\w*?)\/auctions.json/g, "$1")
    let lastModified = apiFiles.files[0].lastModified
    let hashQuery = await trx
      .table("auc_batch")
      .select("id", "enhanced")
      .where("uuid", "=", uuid)
      .andWhere("lastChanged", "=", lastModified)

    if (hashQuery.length)
      return {
        status: "ALREADY_AVAILABLE" as "ALREADY_AVAILABLE",
        batch: hashQuery[0].id,
        enhanced: Boolean(hashQuery[0].enhanced),
      }

    let batch: number[]
    await trx
      .table("auc_batch")
      .where("realms", "=", slug)
      .delete()
    for (let i = 0; i < apiFiles.files.length; i++) {
      const file = apiFiles.files[i]
      let uuid = file.url.replace(/.*?\/auction-data\/(\w*?)\/auctions.json/g, "$1")
      let ahData = await _rawRequest<AHDataResponse>(file.url)
      let mainRealm = ahData.realms.map(r => r.slug).sort((a, b) => {
        if (a > b) return 1
        if (a < b) return -1
        return 0
      })[0]
      console.log("Inserting batch...")
      batch = await trx.table("auc_batch").insert({
        uuid: uuid,
        lastChanged: lastModified,
        realms: mainRealm,
      })

      let auctionsSliced = sliceList(ahData.auctions)

      await Promise.all(auctionsSliced.map(writeAuctions(batch[0], trx)))
    }

    console.log("Batch finished.")
    return {
      status: "PROCESSED" as "PROCESSED",
      batch: batch![0]!,
      enhanced: false,
    }
  }

  /** "for each file" update */
  const writeAuctions = (batchId: number, trx: import("knex").Transaction) => async (
    auctions: AuctionInfo[],
  ) => {
    //let ahData = null
    for (let ahdata_it = 0; ahdata_it < auctions.length; ahdata_it++) {
      const item = auctions[ahdata_it]
      if (item.owner === "???" || item.ownerRealm === "???") continue
      try {
        await trx.table("auc_data_raw").insert({
          batch: batchId,
          auc: item.auc,
          item: item.item,
          owner: item.owner,
          ownerRealm: item.ownerRealm,
          bid: item.bid,
          buyout: item.buyout,
          quantity: item.quantity,
          timeLeft: item.timeLeft,
        })
      } catch (err) {
        //"Cannot add or update a child row: a foreign key constraint fails
        //(`nxtpress`.`auc_data_raw`, CONSTRAINT `FK_auc_data_raw_db_ItemSparse_25632`
        //FOREIGN KEY (`item`) REFERENCES `db_ItemSparse_25632` (`ID`))"

        const itemNotFound =
          err.code === "ER_NO_REFERENCED_ROW_2" &&
          err.sqlMessage &&
          err.sqlMessage.includes("FK_auc_data_raw_db_ItemSparse_25632")
        // aka: if item is not referenced in the game table, dont add it.
        if (itemNotFound) {
          //ok, skip
        } else {
          throw err
        }
      }
    }
  }

  /**
   * one cannot know which faction each auction belongs to from the start.
   * We end up having to query each character in order to know its faction.
   * That query is cached into a characters table.
   * Finally, the main table is "enhanced" with new columns
   */
  async function enhanceBatchFaction(batchId: number, trx: import("knex").Transaction) {
    let count = 0

    console.log("Enhance batch", batchId)
    let batchMeta = await trx
      .table("auc_batch")
      .where({ id: batchId })
      .select()
    if (batchMeta.enhanced) {
      console.log(`batch ${batchId} already enhanced.`)
      return
    }
    let list = await listMissingToonsForEnhanceBatch(batchId, trx)

    if (list.length) {
      {
        // check quota
        let auc = list[0]
        let peekQuota = await bnet.request<any>(
          `/wow/character/${_getSlug(auc.ownerRealm)}/${encodeURIComponent(auc.owner)}`,
          undefined,
          undefined,
          "body_header",
        )
        let alotted = Number(
          peekQuota.headers["x-apikey-quota-allotted"] ||
            peekQuota.headers["x-plan-quota-allotted"],
        )
        let consumed = Number(
          peekQuota.headers["x-apikey-quota-current"] || peekQuota.headers["x-plan-quota-current"],
        )
        let available = alotted - consumed - 1
        if (available <= list.length) {
          throw ono({ code: "NOT_ENOUGH_QUOTA" }, "Not enough request quota for that.")
        }
      }
      console.log(`Fetching character data for ${list.length} characters.`)
      let slices = sliceList(list)
      await Promise.all(slices.map(requestSlice))
    }
    console.log("Updating batch")

    await trx.raw(
      `UPDATE auc_data_raw auc 
        LEFT JOIN api_character chrc ON chrc.name = auc.owner AND chrc.realmName = auc.ownerRealm
        SET auc.faction = chrc.faction
        WHERE 
          auc.batch = ? AND auc.faction IS NULL AND chrc.faction IS NOT NULL;`,
      batchId,
    )
    await trx.raw(
      `UPDATE auc_data_raw auc 
        LEFT JOIN realm ON auc.ownerRealm = realm.name
        SET auc.realmGroup = realm.connected
        WHERE 
          auc.batch = ? AND auc.realmGroup IS NULL;`,
      batchId,
    )
    await trx
      .table("auc_batch")
      .update({ enhanced: 1 })
      .where({ id: batchId })
    console.log("enhanceBatchFaction success for batch", batchId)

    // ---

    function upcount() {
      count = count + 1
      if (count % 20 === 0) console.log("count ", count)
    }

    async function requestSlice(list: NewToons[]) {
      for (let i = 0; i < list.length; i++) {
        const element = list[i]
        await requestToonDetail(element, trx)
        upcount()
      }
    }
  }

  type NewToons = {
    owner: string
    ownerRealm: string
  }

  /** lists toons which will need to be API queried */
  async function listMissingToonsForEnhanceBatch(batch: number, trx: import("knex").Transaction) {
    const [newToons]: NewToons[][] = await trx.raw(
      `SELECT DISTINCT auc.owner, auc.ownerRealm, chrc.name 
      FROM auc_data_raw auc LEFT JOIN api_character chrc 
      ON auc.owner = chrc.name AND auc.ownerRealm = chrc.realmName WHERE batch = ? AND chrc.name IS NULL;`,
      batch,
    )
    console.log("enhanceBatch run", newToons.length, "characters to query")
    return newToons
  }

  async function requestToonDetail(toon: NewToons, trx: import("knex").Transaction) {
    type ToonInfoResp = {
      faction: 0 | 1
      guild: {
        name: string
        realm: string
      }
    }
    const toonSlug = _getSlug(toon.ownerRealm)
    try {
      var toonInfo = await _apiRequest<ToonInfoResp>(
        `/wow/character/${toonSlug}/${encodeURIComponent(toon.owner)}`,
        { fields: "guild" },
        { noLog: true },
      )
    } catch (err) {
      if (err.status && err.status === "nok") {
        await writeToon({ name: toon.owner, realm: toon.ownerRealm, faction: -1 })
        console.log("Error for:", toon)
        return
      }
      throw err
    }

    await writeToon({
      name: toon.owner,
      realm: toon.ownerRealm,
      faction: toonInfo.faction,
    })

    // ---

    function writeToon(i: { name: string; realm: string; faction: 0 | 1 | -1 }) {
      return trx.raw(`REPLACE INTO api_character (name, realmName, faction) VALUES (?, ?, ?)`, [
        i.name,
        i.realm,
        i.faction,
      ])
    }
  }

  return { run }

  // -- util

  function _apiRequest<Response>(
    path: string,
    qs: any = {},
    otherParams: request.CoreOptions & { noLog?: boolean } = {},
  ) {
    return bnet.request<Response>(path, qs, otherParams)
  }

  function _rawRequest<Response>(path: string) {
    console.log("Downloading AH info from", path, new Date().toISOString())
    return new Promise<Response>((resolve, reject) => {
      request({ url: path, json: true }, (err, httpresp, body) => {
        console.log("Finished downloading AH info from", path, new Date().toISOString())
        if (err) return reject(err)
        if (String(httpresp.statusCode).charAt(0) !== "2") {
          console.log("Error in API", httpresp.statusCode, body)
          return reject(body)
        }
        resolve(body)
      })
    })
  }

  function sliceList<T>(list: T[]) {
    let slices = [0, 1, 2, 3, 4].map(i => {
      return list.slice(Math.floor((list.length / 5) * i), Math.floor((list.length / 5) * (i + 1)))
    })
    return slices
  }
}

export type AHLinkResponse = {
  files: { url: string; lastModified: number }[]
}

export type AuctionInfo = {
  auc: number
  item: number
  owner: string
  ownerRealm: string
  bid: number
  buyout: number
  quantity: number
  timeLeft: string //LONG, VERY_LONG ETC
  rand: number
  seed: number
  context: number
}

export type AHDataResponse = {
  realms: { name: string; slug: string }[]
  auctions: AuctionInfo[]
}
