import { NotificationBuilders, Result } from "../common/rulescanner"

export default class {
  constructor(private ctx: Nextpress.Context, private rulescanner: Modules.Rulescanner) {}

  knex = this.ctx.database.db()

  /**
   * Generates results for each user (aka rules + AH data), then sends them to the available
   * channels (email and web push)
   */
  async run() {
    console.log("broadcast results")
    let [users]: [
      {
        id: number
        email: string
        opt_email_notifications: boolean
        opt_browser_notifications: boolean
      }[]
    ] = await this.knex.raw(`
      SELECT user.id, email, opt_email_notifications, opt_browser_notifications 
      FROM
        user JOIN user_extended ON user.id = user_extended.id
      WHERE
        opt_email_notifications IS NOT NULL OR opt_browser_notifications IS NOT NULL;
    `)
    for (let x = 0; x < users.length; x++) {
      const user = users[x]
      try {
        let results = await this.rulescanner.asUser(user.id).run({ addNoDataErrors: false })
        if (!results.length) continue
        if (user.opt_browser_notifications) {
          await this.sendBrowserNotificationsForUser(user.id, results)
        }
        if (user.opt_email_notifications) {
          await this.sendEmailNotificationsForUser(user.email, results)
        }
      } catch (err) {
        console.error("iterateResults error on user", user.id, err)
      }
    }
  }

  async sendBrowserNotificationsForUser(userId: number, results: Result[]) {
    let notification = NotificationBuilders.getWebNotification(results)
    const subs: {
      sessionId: number
      endpoint: string
      auth: string
      p256dh: string
    }[] = await this.knex("push_subscriptions")
      .where("userId", "=", userId)
      .select("sessionId", "endpoint", "auth", "p256dh")
    await Promise.all(
      subs.map(sub => {
        return this.ctx.webpush
          .sendNotification(JSON.stringify({ notification }), {
            auth: sub.auth,
            endpoint: sub.endpoint,
            p256dh: sub.p256dh,
          })
          .catch(err => {
            if (err.statusCode === 410 || err.statusCode === 400) {
              console.log("sub has returned error - deleting it", err)
              return this.knex
                .table("push_subscriptions")
                .where("sessionId", "=", sub.sessionId)
                .delete()
            }
            throw err
          })
      }),
    )
  }

  async sendEmailNotificationsForUser(userEmail: string, results: Result[]) {
    let contents = await new NotificationBuilders.Email(this.ctx).build(userEmail, results)
    await this.ctx.mailgun.sendMail({
      email: userEmail,
      subject: contents.title,
      html: contents.body,
    })
  }
}
