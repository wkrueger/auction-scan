import schedule = require("node-schedule")
import { UserAuth } from "@proerd/nextpress"
import fetchBatch from "./fetch-batch"
import broadcastResults from "./broadcast-results"

export default async function(
  ctx: Nextpress.Context,
  auth: UserAuth,
  rulescanner: Modules.Rulescanner,
  bnet: Modules.BNet,
) {
  let _realmVsSlug = await (async () => {
    let knex = ctx.database.db()
    let list: { slug: string; name: string }[] = await knex.table("realm").select("slug", "name")
    return list.reduce((out, item) => {
      return {
        ...out,
        [item.name]: item.slug,
      }
    }, {}) as { [k: string]: string }
  })()

  function _getSlug(realm: string) {
    return _realmVsSlug[realm]
  }

  /**
   * 1x per hour
   *  - update relevant realms AH data
   *  - perform data cleanups
   *  - send notifications (email and web push)
   */
  async function start() {
    let eventRunning = false
    let event = async () => {
      if (eventRunning) return
      eventRunning = true
      console.log("start job at", new Date().toISOString())
      await auth.routineCleanup()

      await fetchBatch(ctx, bnet, _getSlug).run()
      await new broadcastResults(ctx, rulescanner).run()

      eventRunning = false
      console.log("Finished all on", new Date().toDateString())
    }
    if (process.env.NODE_ENV !== "production") {
      event()
    }
    let j = schedule.scheduleJob({ minute: 0 }, () => event())
    console.log("Next invocation on ", j.nextInvocation())
  }

  return {
    start,
  }
}
