import RuleScannerMod from "./common/rulescanner"
import BnetModule from "./common/bnet"

declare global {
  namespace Modules {
    export type BNet = ReturnType<typeof BnetModule>
    export type Rulescanner = ReturnType<typeof RuleScannerMod>
  }
}
