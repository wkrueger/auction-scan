declare module "ono" {
  type KvPair = { [k: string]: any }

  interface Ono {
    (info: KvPair): Error
    (err: Error, info: KvPair): Error
    (err: Error, info: KvPair, message: string): Error
    (err: Error, message: string): Error
    (info: KvPair, message: string): Error
  }

  const ono: Ono

  export = ono
}

declare module "bcrypt"
declare module "web-push" {
  export function setVapidDetails(email: string, publicKey: string, privateKey: string): void
}

declare namespace T {
  type Value<T> = T[keyof T]
  export type Omit<T, K> = Pick<T, Exclude<keyof T, K>>
  type ExceptType<T> = Omit<T, "type">
}

declare module "handlebars"
declare module "node-schedule"
declare module "inline-css"
