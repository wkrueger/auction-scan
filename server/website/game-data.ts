/**
 * Function which query from our extracted game-data database
 */
import knex = require("knex")
import ono = require("ono")

export const ITEM_DB = "db_ItemSparse_25632"
export const REALM_DB = "realm"

type ItemInfo = { ID: number; Name: string }
type Realm = { slug: string; name: string; connected: string }

export default function gameDataMethods(knex: knex) {
  function trimText(query: string) {
    query = query.trim()
    if (query.length < 3) throw ono({ statusCode: 400, code: "TOO_SHORT" }, "Query text too short.")
    return query
  }

  function addLikes(query: string) {
    if (!/%/g.test(query)) query = "%" + query + "%"
    return query
  }

  async function searchItemByName(query: string): Promise<ItemInfo[]> {
    query = trimText(query)
    query = addLikes(query)
    const found = await knex(ITEM_DB)
      .where("Name", "LIKE", query)
      .orderBy("Name", "asc")
      .limit(15)
    return found
  }

  async function searchItemById(query: number) {
    query = Number(query)
    const found = await knex(ITEM_DB)
      .where("ID", query)
      .limit(1)
    return found
  }

  async function searchRealmByName(query: string): Promise<Realm[]> {
    query = trimText(query)
    query = addLikes(query)
    const found = await knex(REALM_DB)
      .where("name", "LIKE", query)
      .orderBy("name", "asc")
      .limit(15)
    return found
  }

  async function searchRealmBySlug(slug: string): Promise<Realm[]> {
    slug = trimText(slug)
    const found = await knex(REALM_DB)
      .where("slug", "=", slug)
      .limit(1)
    return found
  }

  return {
    searchItemByName,
    searchItemById,
    searchRealmByName,
    searchRealmBySlug,
  }
}
