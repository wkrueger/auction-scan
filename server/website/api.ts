import ono = require("ono")
import { RouteSetupHelper, UserAuth } from "@proerd/nextpress"
import GameDataModule from "./game-data"
import UserModule from "./user"
import RulescannerModule from "../common/rulescanner"

/*
const gatewayMw: RequestHandler = (req, res, next) => {
  try {
    if (!req.session) throw ono({ statusCode: 401 }, "Unauthorized")
    if (!req.session.user) throw ono({ statusCode: 401 }, "Unauthorized")
    res.setHeader("X-User-Id", req.session.user.id)
    res.setHeader("X-User-Email", req.session.user.email)
    next()
  } catch (err) {
    next(err)
  }
}*/

export default async function({
  Setup,
  User,
  GameData,
  auth,
  scanner,
}: {
  Setup: RouteSetupHelper
  GameData: ReturnType<typeof GameDataModule>
  User: ReturnType<typeof UserModule>
  auth: UserAuth
  scanner: ReturnType<typeof RulescannerModule>
}) {
  const { yup, express } = Setup
  const api = await Setup.jsonRoutes(async router => {
    Setup.jsonRouteDict(router, {
      "/item-search": Setup.withMiddleware([auth.throwOnUnauthMw], async req => {
        let items = await (() => {
          if (req.body.name) {
            return GameData.searchItemByName(req.body.name || "")
          } else if (req.body.id) {
            return GameData.searchItemById(req.body.id)
          }
          throw ono({ statusCode: 400 }, "Bad request, expects name or id.")
        })()
        return { items }
      }),
      "/scan": Setup.withMiddleware([auth.throwOnUnauthMw], async req => {
        let results = await scanner.asUser(req.session!.user!.id).run({ addNoDataErrors: true })
        return { results }
      }),
      "/guest-scan": async req => {
        let results = await scanner.genericRun({
          rules: req.body.rules,
          realms: req.body.realms,
          validate: true,
          addNoDataErrors: true,
        })
        return { results }
      },
      "/insert-push-subscription": Setup.withMiddleware(
        [auth.throwOnUnauthMw],
        Setup.withValidation(
          {
            body: yup
              .object({
                endpoint: yup
                  .string()
                  .min(20)
                  .required(),
                auth: yup
                  .string()
                  .min(10)
                  .required(),
                p256dh: yup
                  .string()
                  .min(10)
                  .required(),
              })
              .noUnknown(),
          },
          async req => {
            const user = new User.UserInstance(req.session!.user!.id)
            await user.insertSubscription(
              req.body.endpoint,
              req.sessionID!,
              req.body.auth,
              req.body.p256dh,
            )
            return { status: "OK" }
          },
        ),
      ),
      "/settings/view": Setup.withMiddleware([auth.throwOnUnauthMw], async req => {
        const user = new User.UserInstance(req.session!.user!.id)
        return { settings: await user.getSettings() }
      }),
      "/settings/update": Setup.withMiddleware(
        [auth.throwOnUnauthMw],
        Setup.withValidation(
          {
            body: yup
              .object({
                settings: yup.mixed().required(),
              })
              .noUnknown(),
          },
          async req => {
            const user = new User.UserInstance(req.session!.user!.id)
            await user.updateSettings(req.body.settings)
            return { status: "OK" }
          },
        ),
      ),
    })

    // -- rules entity --
    const rules = express.Router()
    rules.use(auth.throwOnUnauthMw)
    Setup.jsonRouteDict(rules, {
      "/list": async req => {
        const user = new User.UserInstance(req.session!.user!.id)
        let list = await user.listRules()
        return { rules: list }
      },
      "/remove": Setup.withValidation({ body: yup.object({ id: yup.number() }) }, async req => {
        const user = new User.UserInstance(req.session!.user!.id)
        await user.removeRule(req.body.id)
        return { status: "OK" }
      }),
      "/create": async req => {
        const user = new User.UserInstance(req.session!.user!.id)
        const id = await user.insertRule(req.body)
        return { id }
      },
    })
    router.use("/rule", rules)

    // -- realms entity --

    const realms = express.Router()
    realms.use(auth.throwOnUnauthMw)
    Setup.jsonRouteDict(realms, {
      "/list": async req => {
        const user = new User.UserInstance(req.session!.user!.id)
        let list = await user.listRealms()
        return { realms: list }
      },
      "/remove": Setup.withValidation({ body: yup.object({ slug: yup.string() }) }, async req => {
        const user = new User.UserInstance(req.session!.user!.id)
        await user.removeRealm(req.body.slug)
        return { status: "OK" }
      }),
      //slug alliance horde
      "/create": async req => {
        const user = new User.UserInstance(req.session!.user!.id)
        const id = await user.insertRealm(req.body)
        return { id }
      },
      "/search": async req => {
        let realms = await (() => {
          if (req.body.name) {
            return GameData.searchRealmByName(req.body.name || "")
          } else if (req.body.slug) {
            return GameData.searchRealmBySlug(req.body.slug)
          }
          throw ono({ statusCode: 400 }, "Bad request, expects name or slug.")
        })()
        return { realms: realms.map(r => ({ ID: r.slug, Name: r.name, connected: r.connected })) }
      },
    })
    router.use("/realm", realms)
  })

  return api
}
