import { Server, RequestHandler, UserAuth } from "@proerd/nextpress"
import UserModule from "./user"
import ApiModule from "./api"
import GameDataModule from "./game-data"
import ScannerModule from "../common/rulescanner"
import { resolve } from "path"

export default async (
  ctx: Nextpress.Context,
  User: ReturnType<typeof UserModule>,
  scanner: ReturnType<typeof ScannerModule>,
) => {
  const knex = ctx.database.db()
  const server = new Server(ctx)

  const auth = new UserAuth(ctx)
  await auth.init()

  server.routeSetup = async ({ app, helper: setup }) => {
    let authRoute = await auth.userRoutes(setup)
    const authRedirectMw: RequestHandler = (req, res, next) => {
      try {
        auth.checkSession(req, res)
        next()
      } catch {
        res.redirect("/unauth")
      }
    }

    app.get("/service-worker.js", (_, res) => {
      res.sendFile(resolve(ctx.projectRoot, "service-worker.js"))
    })

    const authGroup = await setup.jsonRoutes(async router => {
      router.use(authRoute.json)
    })
    app.use("/user", authGroup)

    const api = await ApiModule({
      Setup: setup,
      User,
      GameData: GameDataModule(knex),
      auth,
      scanner,
    })
    app.use("/api", api)

    const html = await setup.htmlRoutes(async router => {
      // root route
      router.get("/", authRedirectMw, (_, res) => {
        return res.redirect("/dashboard")
      })

      // dashboard
      router.get(
        "/dashboard",
        authRedirectMw,
        setup.tryMw((req, res) => {
          return setup.nextApp().render(req, res, "/dashboard", {
            user: { email: req.session!.user!.email, id: req.session!.user!.id },
          } as any)
        }),
      )
      router.use(authRoute.html)
    })
    app.use(html)
  }

  server.run()
}

declare global {
  namespace Express {
    interface SessionData {
      user: { id: number; email: string } | undefined
    }
  }
}
