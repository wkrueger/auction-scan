import Yup = require("yup")
import { _WatchRules, validateRule } from "../common/rulescanner"
import { ITEM_DB } from "./game-data"
type Rule = _WatchRules[keyof _WatchRules]

type SchemaType<T> = T extends Yup.ObjectSchema<infer Y> ? Y : never

export default function(ctx: Nextpress.Context) {
  const realmSchema = Yup.object({
    slug: Yup.string().required(),
    alliance: Yup.boolean().required(),
    horde: Yup.boolean().required(),
  })

  const knex = ctx.database.db()

  type RealmSub = SchemaType<typeof realmSchema>

  class UserInstance {
    constructor(public id: number) {
      if (!id) throw Error("Invalid user id.")
    }

    static guestUserId?: number = undefined
    static async getGuestUserId() {
      if (this.guestUserId === undefined) {
        let [id] = await knex("user")
          .select("id")
          .where({ email: "guest@guest.com" })
        this.guestUserId = id
      }
      return this.guestUserId!
    }

    async notGuestCheck() {
      if (UserInstance.guestUserId === undefined) {
        let [id] = await knex("user")
          .select("id")
          .where({ email: "guest@guest.com" })
        UserInstance.guestUserId = id
      }

      if (this.id === UserInstance.guestUserId) throw Error("Operation not available as guest.")
    }

    async listRules(): Promise<Rule[]> {
      await this.notGuestCheck()
      let rules = await knex("rule")
        .leftJoin(ITEM_DB, function() {
          this.on("rule.rule_item_id", "=", `${ITEM_DB}.ID`)
        })
        .where("userId", this.id)
        .select(
          "rule.id",
          "rule.rule_type",
          "rule.rule_value",
          "rule.rule_item_id",
          `${ITEM_DB}.Name`,
        )
      return rules.map((o: any) => ({
        id: o.id,
        type: o["rule_type"],
        value: o["rule_value"],
        itemId: o["rule_item_id"],
        _itemName: o["Name"],
      }))
    }

    async insertRule(rule: Rule) {
      await this.notGuestCheck()
      //fixme validate
      const validated = validateRule(rule)
      return knex("rule").insert({
        userId: this.id,
        rule_type: validated.type,
        rule_value: validated.value,
        rule_item_id: validated.itemId,
      })
    }

    async removeRule(id: number) {
      await this.notGuestCheck()
      return knex("rule")
        .where({ userId: this.id, id })
        .delete()
    }

    async listRealms() {
      await this.notGuestCheck()
      let realms: any[] = await knex("realm_sub")
        .where("userId", this.id)
        .leftJoin("realm", function() {
          this.on("realm_sub.realmSlug", "=", "realm.slug")
        })
        .select(
          "realm_sub.id",
          "realm_sub.realmSlug",
          "realm_sub.factions",
          "realm.name",
          "realm.connected",
        )
      return realms.map(r => {
        let out = {
          id: r.id as number,
          slug: r.realmSlug as string,
          horde: (Number(r.factions) & 1) === 1,
          alliance: (Number(r.factions) & 2) === 2,
          name: r.name as string,
          connected: r.connected as string,
        }
        return out
      })
    }

    async removeRealm(slug: string) {
      await this.notGuestCheck()
      if (!slug) throw Error("Invalid slug")
      return knex("realm_sub")
        .where({
          userId: this.id,
          realmSlug: slug,
        })
        .delete()
    }

    async insertRealm(inp: RealmSub) {
      await this.notGuestCheck()
      const validated = realmSchema.validateSync(inp)
      let factionMap = 0
      if (validated.alliance) factionMap |= 2
      if (validated.horde) factionMap |= 1
      if (factionMap === 0) throw Error("Select at least one faction!")
      return knex("realm_sub").insert({
        userId: this.id,
        realmSlug: validated.slug,
        factions: factionMap,
      })
    }

    async insertSubscription(endpoint: string, sessionId: string, authStr: string, p256dh: string) {
      await this.notGuestCheck()
      return knex.raw(
        `REPLACE INTO push_subscriptions(sessionId, userId, endpoint, auth, p256dh) VALUES (?, ?, ?, ?, ?);`,
        [sessionId, this.id, endpoint, authStr, p256dh],
      )
    }

    static userSettingsSchema = Yup.object({
      optBrowserNotifications: Yup.boolean().required(),
      optEmailNotifications: Yup.boolean().required(),
    }).noUnknown()

    async updateSettings(i: SchemaType<typeof UserInstance["userSettingsSchema"]>) {
      await this.notGuestCheck()
      i = UserInstance.userSettingsSchema.validateSync(i)
      let found = await knex("user_extended")
        .select("id")
        .where("id", "=", this.id)
      if (!found.length) {
        await knex("user_extended").insert({ id: this.id })
      }

      if (!i.optBrowserNotifications) {
        await knex("push_subscriptions")
          .where("userId", "=", this.id)
          .delete()
      }

      await knex("user_extended")
        .where("id", "=", this.id)
        .update({
          opt_email_notifications: i.optEmailNotifications ? 1 : null,
          opt_browser_notifications: i.optBrowserNotifications ? 1 : null,
        })
    }

    async getSettings(): Promise<SchemaType<typeof UserInstance["userSettingsSchema"]>> {
      await this.notGuestCheck()
      let items: any[] = await knex("user_extended")
        .select("opt_email_notifications", "opt_browser_notifications")
        .where("id", "=", this.id)
      if (!items.length) {
        const defaultSet = { optBrowserNotifications: false, optEmailNotifications: false }
        await this.updateSettings(defaultSet)
        return defaultSet
      }
      return {
        optEmailNotifications: Boolean(items[0].opt_email_notifications),
        optBrowserNotifications: Boolean(items[0].opt_browser_notifications),
      }
    }
  }

  return {
    UserInstance,
  }
}
