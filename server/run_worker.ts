import path = require("path")
import KnexModule from "./common/knex"
import WorkerModule from "./worker"
import * as contextMappers from "./common/context-mappers"
import { ContextFactory, defaultMappers, UserAuth } from "@proerd/nextpress"
import UserMod from "./website/user"
import RuleScannerMod from "./common/rulescanner"
import BnetModule from "./common/bnet"
//
;(async () => {
  const context = ContextFactory({
    mappers: [
      defaultMappers.website,
      defaultMappers.mailgun,
      defaultMappers.database,
      contextMappers.battleNet,
      contextMappers.webpush,
    ],
    projectRoot: path.resolve(__dirname, ".."),
  })
  const auth = new UserAuth(context)
  await KnexModule(context)
  const user = UserMod(context)
  const rulescanner = RuleScannerMod(context, user)
  const bnet = BnetModule(context)
  let worker = await WorkerModule(context, auth, rulescanner, bnet)
  worker.start()
})()

declare global {
  namespace Nextpress {
    interface CustomContext {
      apiKey: string
    }
  }

  export interface User {
    id: number
    email: string
    auth?: string
    validationHash?: string
  }
}
