import request = require("request")

function waitP(time: number, fn = () => {}) {
  return new Promise(res => {
    setTimeout(() => {
      fn()
      res()
    }, time)
  })
}

// let _lastRequest = 0
// const THROTTLE = 100
// async function gatewayThrottle() {
//   let now = new Date().getTime()
//   if (now - _lastRequest > THROTTLE) {
//     _lastRequest = now
//     return
//   } else {
//     return new Promise(res => {
//       setTimeout(() => {
//         _lastRequest = new Date().getTime()
//         res()
//       }, now - _lastRequest + Math.ceil(THROTTLE / 2))
//     })
//   }
// }

const bnet = (ctx: Nextpress.Context) => {
  return {
    request: doRequest(ctx),
  }
}

export default bnet

const doRequest = (ctx: Nextpress.Context) => async <Resp>(
  path: string,
  qs: { [k: string]: string } = {},
  _otherParams: Partial<request.Options> & { noLog?: boolean } = {},
  returnType: "bodyonly" | "body_header" = "bodyonly",
) => {
  let otherParams = _otherParams as request.Options
  //await gatewayThrottle()
  if (!_otherParams.noLog) console.log("callApi", path)
  if (path.charAt(0) != "/") path = "/" + path
  return new Promise<Resp>((resolve, reject) => {
    request(
      {
        url: `https://us.api.battle.net${path}`,
        json: true,
        qs: {
          ...qs,
          locale: "en_US",
          apikey: ctx.apiKey,
        },
        ...otherParams,
      },
      async (err, httpresp, body) => {
        if (err) return reject(err)
        if (String(httpresp.statusCode).charAt(0) !== "2") {
          console.log("Error in API", httpresp.statusCode, body)
          if (String(httpresp.statusCode) === "504") {
            try {
              await waitP(2000)
              let r = await doRequest(ctx)(path, qs, otherParams)
              return resolve(r as any)
            } catch (err) {
              return reject(err)
            }
          }
          return reject(body)
        }
        // let pickKeys = Object.keys(httpresp.headers).filter(
        //   h => h.endsWith("-current") || h.endsWith("-allotted"),
        // )
        //let filtered = pickKeys.reduce((o, key) => ({ ...o, [key]: httpresp.headers[key] }), {})
        //console.log("quota headers", filtered)
        if (returnType === "bodyonly") {
          resolve(body)
        } else if (returnType === "body_header") {
          resolve({
            body,
            headers: httpresp.headers,
          } as any)
        }
      },
    )
  })
}

type RealmResp = {
  realms: {
    type: "pvp" | "pve"
    population: string
    queue: boolean
    status: boolean
    name: string //Aegwynn
    slug: string //aegwynn
    battlegroup: string // Vengeance
    locale: string //"en_US",
    timezone: string //"America/Chicago",
    connected_realms: string[] //["daggerspine", "bonechewer", "gurubashi", "hakkar", "aegwynn"]
  }[]
}

export class RealmList {
  constructor(public _bnet: ReturnType<typeof bnet>) {}
  static slugList: { name: string; slug: string }[]

  async fetch() {
    if (RealmList.slugList) return RealmList.slugList
    let res = await this._bnet.request<RealmResp>("/wow/realm/status")
    RealmList.slugList = res.realms.map(r => ({ name: r.name, slug: r.slug }))
    return RealmList.slugList
  }
}
