import ono = require("ono")
import yup = require("yup")
import UserModule from "../website/user"
import handlebars = require("handlebars")
import { readFile } from "fs"
import { promisify } from "util"
import { resolve } from "path"
import inlineCss = require("inline-css")

enum FACTIONS {
  Alliance,
  Horde,
}

export type Result = {
  type: string
  slug: string
  realmName: string
  faction: 0 | 1
  itemId: number
  value: number
  stockFound?: number
  minimumFound?: number
  _itemName?: string
}

export default function(ctx: Nextpress.Context, User: ReturnType<typeof UserModule>) {
  const knex = ctx.database.db()

  let _realmVsSlug: any
  async function _getRealmName(slug: string) {
    if (!_realmVsSlug) {
      _realmVsSlug = await (async () => {
        let knex = ctx.database.db()
        let list: { slug: string; name: string }[] = await knex
          .table("realm")
          .select("slug", "name")
        return list.reduce((out, item) => {
          return {
            ...out,
            [item.slug]: item.name,
          }
        }, {}) as { [k: string]: string }
      })()
    }
    return _realmVsSlug[slug]
  }

  function _checkscanners<
    T extends {
      [k in keyof _WatchRules]: (
        rule: _WatchRules[k],
        slug: string,
        faction: 0 | 1,
      ) => Promise<false | (_WatchRules[k] & { slug: string; realmName: string })>
    }
  >(i: T): T {
    return i
  }

  const scanners = _checkscanners({
    async minvalueIsBelow(rule, slug, faction) {
      let minValue: any[] = await knex
        .table("auc_data_raw")
        .min("buyout as buyout")
        .as("buyout")
        .select("item")
        .where("item", "=", rule.itemId)
        .andWhere("realmGroup", "=", slug)
        .andWhere("faction", "=", faction)
      if (!minValue.length) return false
      if (golds(minValue[0].buyout) > rule.value) return false
      return {
        ...rule,
        minimumFound: minValue[0].buyout,
        slug,
        realmName: await _getRealmName(slug),
        faction,
      }
    },
    async minvalueIsOver(rule, slug, faction) {
      let minValue: any[] = await knex
        .table("auc_data_raw")
        .min("buyout as buyout")
        .as("buyout")
        .select("item")
        .where("item", "=", rule.itemId)
        .andWhere("realmGroup", "=", slug)
        .andWhere("faction", "=", faction)
      if (!minValue.length) return false
      if (golds(minValue[0].buyout) <= rule.value) return false
      return {
        ...rule,
        minimumFound: minValue[0].buyout,
        slug,
        realmName: await _getRealmName(slug),
        faction,
      }
    },
    async stockIsBelow(rule, slug, faction) {
      let stock: any[] = await knex
        .table("auc_data_raw")
        .sum("quantity as quantity")
        .select("item")
        .where("item", "=", rule.itemId)
        .andWhere("realmGroup", "=", slug)
        .andWhere("faction", "=", faction)
      if (!stock.length)
        return {
          ...rule,
          stockFound: 0,
          slug,
          realmName: await _getRealmName(slug),
          faction,
        }
      if (stock[0].quantity >= rule.value) return false
      return {
        ...rule,
        stockFound: stock[0].quantity,
        slug,
        faction,
        realmName: await _getRealmName(slug),
      }
    },
  })

  type Rule = {
    type: string
    itemId: number
    value: number
  }

  type SchemaType<T> = T extends yup.ObjectSchema<infer Y> ? Y : never

  const realmSchema = yup
    .object({
      slug: yup.string().required(),
      name: yup.string().required(),
      connected: yup.string().required(),
      alliance: yup.boolean().required(),
      horde: yup.boolean().required(),
    })
    .noUnknown()

  type RealmSetting = SchemaType<typeof realmSchema>

  async function genericRun({
    rules,
    realms,
    validate,
    addNoDataErrors,
  }: {
    rules: Rule[]
    realms: RealmSetting[]
    validate: boolean
    addNoDataErrors: boolean
  }): Promise<Result[]> {
    if (validate) {
      for (let x = 0; x < rules.length; x++) {
        const rule = rules[x] as any
        validateRule(rule)
      }
      for (let x = 0; x < realms.length; x++) {
        const realm = realms[x]
        realmSchema.validateSync(realm)
      }
    }

    let results = [] as any[]

    for (let i = 0; i < realms.length; i++) {
      const realmInfo = realms[i]
      const hasRealmData = (await knex("auc_batch")
        .select("id")
        .where("realms", "=", realmInfo.connected)).length
      if (hasRealmData) {
        let alliance = realmInfo.alliance
          ? await runRealmFaction(realmInfo.slug, FACTIONS.Alliance)
          : []
        let horde = realmInfo.horde ? await runRealmFaction(realmInfo.slug, FACTIONS.Horde) : []
        let combined = [...alliance, ...horde].filter(r => r)
        results = [...results, ...combined]
      } else if (addNoDataErrors) {
        if (realmInfo.alliance) {
          results.push({
            type: "noDataAvailableForRealm",
            realmName: realmInfo.name,
            slug: realmInfo.slug,
            faction: FACTIONS.Alliance,
          } as Result)
        }
        if (realmInfo.horde) {
          results.push({
            type: "noDataAvailableForRealm",
            realmName: realmInfo.name,
            slug: realmInfo.slug,
            faction: FACTIONS.Horde,
          } as Result)
        }
      }
    }

    return results

    // ---

    function runRealmFaction(slug: string, faction: 0 | 1) {
      return Promise.all(
        rules.map(rule => {
          let scanner: any = (scanners as any)[rule.type]
          if (!scanner) throw Error(`Scanner not found for type ${rule.type}`)
          return scanner(rule, slug, faction)
        }),
      )
    }
  }

  function asUser(userId: number) {
    let user = new User.UserInstance(userId)

    async function run(i: { addNoDataErrors: boolean }): Promise<Result[]> {
      let rules = await user.listRules()
      let realms = await user.listRealms()
      return genericRun({ rules, realms, validate: false, addNoDataErrors: i.addNoDataErrors })
    }

    return { run }
  }

  return { asUser, genericRun }
}

const yuptypes = {
  minvalueIsBelow: yup
    .object({
      sort: yup.number(),
      type: yup.string().required(),
      itemId: yup.number().required(),
      value: yup.number().required(),
    })
    .noUnknown(),
  minvalueIsOver: yup
    .object({
      sort: yup.number(),
      type: yup.string(),
      itemId: yup.number(),
      value: yup.number(),
    })
    .noUnknown(),
  stockIsBelow: yup
    .object({
      sort: yup.number(),
      type: yup.string(),
      itemId: yup.number(),
      value: yup.number(),
    })
    .noUnknown(),
}

export function validateRule<T extends _WatchRules[keyof _WatchRules]>(r: T): T {
  let rule = yuptypes[r.type as keyof typeof yuptypes]
  if (!rule) {
    throw ono({ code: "ValidationError" }, 'Missing or invalid "type" key.')
  }
  return (rule.validateSync as any)(r)
}

export type _WatchRules = {
  minvalueIsBelow: {
    id?: number
    type: "minvalueIsBelow"
    itemId: number
    value: number
    minimumFound?: number
  }
  minvalueIsOver: {
    id?: number
    type: "minvalueIsOver"
    itemId: number
    value: number
    minimumFound?: number
  }
  stockIsBelow: {
    id?: number
    type: "stockIsBelow"
    itemId: number
    value: number
    stockFound?: number
  }
  /*
  timeInterval: {
    id?: number
    type: "timeInterval"
    weekdayFilter: number[] | undefined //0 to 6
    dayHourFiler: number[] | undefined //0 to 23
  }*/
}

function golds(coppers: number) {
  return coppers / 10000
}

function goldsDisplay(coppers?: number) {
  return golds(coppers || 0).toFixed(0) + "g"
}

export type WatchRules = T.Value<_WatchRules>

export namespace NotificationBuilders {
  const factionMappings = {
    0: "Alliance",
    1: "Horde",
  }
  const typeMappings = {
    minvalueIsBelow(result: Result) {
      return (
        `Value of ${result._itemName} is below ${result.value} ` +
        `(found: ${goldsDisplay(result.minimumFound)}) on ${result.realmName} - ${
          factionMappings[result.faction]
        }.`
      )
    },
    minvalueIsOver(result: Result) {
      return (
        `Value of ${result._itemName} is over ${result.value} ` +
        `(found: ${goldsDisplay(result.minimumFound)}) on ${result.realmName} - ${
          factionMappings[result.faction]
        }.`
      )
    },
    stockIsBelow(result: Result) {
      return (
        `Stock of ${result._itemName} is below ${result.value} units ` +
        `(found: ${result.stockFound}) on ${result.realmName} - ${factionMappings[result.faction]}.`
      )
    },
  }

  export function getNotificationText(result: Result) {
    let mapping = (typeMappings as any)[result.type]
    return mapping ? (mapping(result) as string) : ""
  }

  export function getWebNotification(results: Result[]) {
    results = results.filter(r => r.type !== "noDataAvailableForRealm")
    let body = results
      .slice(0, 1)
      .map(getNotificationText)
      .join("\n\n")
    if (results.length > 1)
      body += `\n\n...and other ${results.length - 1} deals found. (click for details)`
    return {
      title: "Auction deals found!",
      content: {
        icon: "/static/notification-icon.jpg",
        badge: "/static/notification-badge-72.png",
        body,
      },
    }
  }

  export class Email {
    constructor(private ctx: Nextpress.Context) {}

    static _template?: Function
    static async getTemplate(ctx: Nextpress.Context) {
      if (this._template) return this._template
      let str = String(
        await promisify(readFile)(resolve(ctx.projectRoot, "static", "email-template.hbs")),
      )
      str = await inlineCss(str, { url: ctx.website.root })
      this._template = handlebars.compile(str)
      return this._template!
    }

    async build(email: string, _results: Result[]) {
      _results = _results.filter(r => r.type !== "noDataAvailableForRealm")
      let templ = await Email.getTemplate(this.ctx)
      const results = _results.map(result => {
        let ruleText = (typeMappings as any)[result.type] || (() => "")
        return {
          itemName: result._itemName || result.itemId,
          ruleText: ruleText(result),
        }
      })
      let body = templ({
        websiteLink: this.ctx.website.root + "/dashboard?tab=scan",
        unsubLink: this.ctx.website.root + "/dashboard?tab=options",
        results,
      })
      return {
        title: "You'be got deals",
        target: email,
        body,
      }
    }
  }
}
