import { ContextMapper } from "@proerd/nextpress/built/server/context"

export const battleNet: ContextMapper = {
  id: "battlenet",
  envKeys: ["BNET_API_KEY"],
  optionalKeys: [],
  envContext: () => ({
    apiKey: process.env.BNET_API_KEY!,
  }),
}

export const webpush: ContextMapper = {
  id: "webpush",
  envKeys: ["WEBPUSH_PUBLIC_KEY", "WEBPUSH_PRIVATE_KEY", "WEBPUSH_MAILTO"],
  optionalKeys: [],
  envContext: () => ({
    webpush: {
      publicKey: process.env.WEBPUSH_PUBLIC_KEY,
      privateKey: process.env.WEBPUSH_PRIVATE_KEY,
      mailTo: process.env.WEBPUSH_MAILTO,
      _wp: undefined as any,
      init() {
        this._wp = require("web-push")
        this._wp.setVapidDetails(this.mailTo, this.publicKey, this.privateKey)
      },
      sendNotification(
        dataToSend: string,
        subscription: {
          endpoint: string
          auth: string
          p256dh: string
        },
      ) {
        if (!this._wp) this.init()
        return this._wp
          .sendNotification(
            {
              endpoint: subscription.endpoint,
              options: {
                applicationServerKey: this.publicKey,
                userVisibleOnly: true,
              },
              keys: { auth: subscription.auth, p256dh: subscription.p256dh },
            },
            dataToSend,
          )
          .catch((err: any) => {
            console.error("Error while sending push notification.")
            throw err
          })
      },
    },
  }),
}

declare global {
  namespace Nextpress {
    interface CustomContext {
      apiKey: string
      webpush: {
        publicKey: string
        privateKey: string
        mailTo: string
        sendNotification(
          dataToSend: string,
          subscription: {
            endpoint: string
            auth: string
            p256dh: string
          },
        ): Promise<void>
      }
    }
  }
}
