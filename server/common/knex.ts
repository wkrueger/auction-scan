import knex = require("knex")
import BnetModule from "./bnet"
import { resolve } from "path"
import { promisify } from "util"
import { readFile } from "fs"
import { UserAuth, Server } from "@proerd/nextpress"

export default async function(ctx: Nextpress.Context) {
  async function readSql(filename: string) {
    let buf = await promisify(readFile)(resolve(ctx.projectRoot, "sql", filename))
    return String(buf)
  }

  async function migrate(trx: knex.Transaction, oldVersion: number, newVersion: number) {
    const auth = new UserAuth(ctx)
    //never send mail on auth operations while used here.
    auth.sendMail = async (...i: any[]) => {}

    if (oldVersion <= 1) {
      //create user table
      await auth.init()

      //force the creation of the session table
      const server = new Server(ctx)
      const sessionStore = server.createSessionStore()
      await new Promise((res, rej) => {
        sessionStore.createDatabaseTable((err: any) => {
          if (err) return rej(err)
          return res()
        })
      })

      await trx.schema.createTable("rule", table => {
        table.increments()
        table
          .integer("userId")
          .unsigned()
          .notNullable()
        table
          .foreign("userId")
          .references("user.id")
          .onDelete("CASCADE")
        table
          .integer("sort")
          .notNullable()
          .defaultTo(0)
        //json not supported by knex
        table.string("rule_type").notNullable()
        table
          .integer("rule_item_id")
          .notNullable()
          .index()
        table.integer("rule_value")
      })

      await trx.schema.createTable("realm", table => {
        table.string("slug", 50).primary()
        table
          .string("name", 50)
          .notNullable()
          .index()
        table.string("connected", 50).notNullable()
        //tried adding index to those columns gave me "foreign constraint error WTF"
      })
      let realms = (await BnetModule(ctx).request<{
        realms: { name: string; slug: string; connected_realms: string[] }[]
      }>("/wow/realm/status")).realms
      for (let x = 0; x < realms.length; x++) {
        const realm = realms[x]
        await trx("realm").insert({
          slug: realm.slug,
          name: realm.name,
          connected: realm.connected_realms.sort()[0],
        })
      }

      await trx.schema.createTable("realm_sub", table => {
        table.increments()
        table
          .integer("userId")
          .unsigned()
          .notNullable()
        table
          .foreign("userId")
          .references("user.id")
          .onDelete("CASCADE")
        table.string("realmSlug", 50).notNullable()
        table.foreign("realmSlug").references("realm.slug")
        table
          .integer("factions")
          .notNullable()
          .defaultTo(0)
        table.unique(["userId", "realmSlug"])
      })

      await trx.schema.raw(await readSql("auc_batch.sql"))
      await trx.schema.raw(await readSql("auc_data_raw.sql"))
      await trx.schema.raw(await readSql("api_character.sql"))
    }

    if (oldVersion < 4) {
      await trx.schema.createTable("user_extended", table => {
        table
          .integer("id")
          .unsigned()
          .primary()
        table
          .foreign("id")
          .references("user.id")
          .onDelete("CASCADE")
        table.boolean("opt_email_notifications")
        table.boolean("opt_browser_notifications")
      })
    }

    if (oldVersion < 8) {
      await trx.schema.createTable("push_subscriptions", table => {
        ;(<any>table)
          .string("sessionId", 128)
          .notNullable()
          .primary()
          .collate("utf8mb4_bin")
        table
          .foreign("sessionId")
          .references("sessions.session_id")
          .onDelete("CASCADE")
        table
          .integer("userId")
          .unsigned()
          .notNullable()
        table
          .foreign("userId")
          .references("user.id")
          .onDelete("CASCADE")
        table.string("endpoint", 500).notNullable()
        table.string("auth", 50).notNullable()
        table.string("p256dh", 128).notNullable()
        table
          .timestamp("createdAt")
          .notNullable()
          .defaultTo(ctx.database.db().fn.now())
      })
    }

    if (oldVersion < 9) {
      await auth.create(
        { email: "guest@guest.com", password: "guesthasnopAssword" },
        { askForValidation: false },
      )
    }
  }

  await ctx.database.init({
    currentVersion: 9,
    migration: migrate,
  })
}
