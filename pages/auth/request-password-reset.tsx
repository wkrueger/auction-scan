import React from "react"
import { url, JsonForm } from "../../app/common/site"

export default function(props: { content: string; title: string }) {
  return (
    <>
      <style global jsx>{`
        body.htmlbody {
          height: 100vh;
          background-image: url(${url("/static/wallhaven-191749.png")});
          background-position: top right;
          background-repeat: no-repeat;
          background-color: #c8c7a9;
        }

        body > div#__next {
          height: 100vh;
        }

        .v-aligner {
          height: 100%;
          display: flex;
          justify-content: center;
          align-items: center;
        }
      `}</style>

      <div className="container v-aligner">
        <div className="card text-center border-dark">
          <div className="card-body">
            <h4 className="card-title">Password reset</h4>
            <JsonForm
              id="pwdForm"
              url={url("/user/request-password-reset")}
              onSuccess={({ setMessage, resetForm }) => {
                setMessage("Request successfully sent. Check your email.")
                resetForm()
              }}
              render={info => (
                <>
                  <JsonForm.AlertBox message={info.message} onHideError={info.hide} />
                  <div className="form-group">
                    <label htmlFor="email">Email</label>
                    <input type="email" name="email" id="email" className="form-control" required />
                  </div>
                  <JsonForm.SubmitButton state={info.state} idleText="Request" />
                </>
              )}
            />
          </div>
        </div>
      </div>
    </>
  )
}
