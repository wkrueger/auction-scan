import React from "react"
import { url, JsonForm } from "../../app/common/site"

export default function(props: { requestId: string }) {
  if (!props.requestId) return <span>Invalid request id.</span>

  return (
    <>
      <style global jsx>{`
        body.htmlbody {
          height: 100vh;
          background-image: url(${url("/static/wallhaven-191749.png")});
          background-position: top right;
          background-repeat: no-repeat;
          background-color: #c8c7a9;
        }

        body > div#__next {
          height: 100vh;
        }

        .v-aligner {
          height: 100%;
          display: flex;
          justify-content: center;
          align-items: center;
        }
      `}</style>

      <div className="container v-aligner">
        <div className="card text-center border-dark">
          <div className="card-body">
            <h4 className="card-title">Password reset</h4>
            <JsonForm
              id="pwdForm"
              url={url("/user/perform-password-reset")}
              onSuccess={({ setMessage, resetForm }) => {
                setMessage("Password reset successful.")
                resetForm()
              }}
              render={info => (
                <>
                  <JsonForm.AlertBox message={info.message} onHideError={info.hide} />
                  <div className="form-group">
                    <label htmlFor="pwd1">New password</label>
                    <input
                      type="password"
                      name="pwd1"
                      id="pwd1"
                      className="form-control"
                      required
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="pwd2">Confirm new password</label>
                    <input
                      type="password"
                      name="pwd2"
                      id="pwd2"
                      className="form-control"
                      required
                    />
                  </div>
                  <input type="hidden" name="requestId" value={props.requestId} />
                  <JsonForm.SubmitButton state={info.state} idleText="Request" />
                </>
              )}
            />
          </div>
        </div>
      </div>
    </>
  )
}
