import React from "react"

import SimpleMessage from "../../app/comps/simple-message"

export default class extends React.Component<{ title: string; content: string }> {
  static getInitialProps({ query }: any) {
    return { title: query.title, content: query.content }
  }

  render() {
    return <SimpleMessage title={this.props.title} content={this.props.content} />
  }
}
