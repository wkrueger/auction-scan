import React from "react"
import { url } from "../app/common/site"
import Markdown from "react-markdown"
import fetch from "isomorphic-fetch"

const Comp: React.SFC<{ title: string }> = props => {
  return (
    <>
      <style global jsx>{`
        body.htmlbody {
          height: 100vh;
          background-image: url(${url("/static/wallhaven-191749.png")});
          background-position: top right;
          background-repeat: no-repeat;
          background-color: #c8c7a9;
        }

        h1 {
          text-align: center;
          margin: 30px !important;
        }
      `}</style>

      <div className="container">
        <div className="section vertical">
          <h1>{props.title}</h1>
        </div>
        <div className="section vertical">
          <div className="card text-center border-dark">
            <div className="card-body">
              <div className="card-text text-left">{props.children}</div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default class extends React.Component<{ text: string }> {
  static async getInitialProps() {
    try {
      var resp = await fetch(url("/static/policy.md"))
      var text = await resp.text()
      return { text }
    } catch (err) {
      console.error("error initialprops", err)
    }
  }

  render() {
    return (
      <Comp title="Privacy policy">
        <Markdown source={this.props.text || "Loading..."} />
      </Comp>
    )
  }
}
