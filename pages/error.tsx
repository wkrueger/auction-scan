import React from "react"
import SimpleMessage from "../app/comps/simple-message"

export default (_props: any) => {
  const props = { ..._props, query: _props.query || {} }
  return <SimpleMessage content={props.query.message} title="Error" />
}
