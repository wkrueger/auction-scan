declare module "next/app"
interface WowheadPower {
  hideTooltip(): void
}

declare var $WowheadPower: WowheadPower
interface Window {
  __REDUX_DEVTOOLS_EXTENSION__: any
  __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any
  whTooltips: any
}

declare namespace Redutser {
  interface DispatchInput<A, S> {
    thunk: ThunkDispatch<A, S>
  }
}

declare namespace T {
  export type Rule_Client = {
    id?: number
    itemId?: number
    _itemName?: string
    type?: string
    value?: string
  }

  export type ScanResult = {
    type: string
    slug: string
    realmName: string
    faction: 0 | 1
    itemId: number
    value: number
    _itemName: string
    stockFound?: number
    minimumFound?: number
  }

  export type NewRealm = {
    name: string
    slug: string
    alliance: boolean
    horde: boolean
    connected?: string
  }
}
