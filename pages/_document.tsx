import Document, { Head, Main, NextScript } from "next/document"
import { url } from "../app/common/site"
import React from "react"

export default class MyDocument extends Document {
  render() {
    return (
      <html>
        <Head>
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <link rel="stylesheet" href={url("/static/custom.css")} />
          <link href="https://fonts.googleapis.com/css?family=Caveat" rel="stylesheet" />
          <link rel="stylesheet" type="text/css" media="screen" href={url("/static/main.css")} />
          <link rel="stylesheet" href="/_next/static/style.css" />
          <link
            rel="icon"
            type="image/png"
            sizes="400x400"
            href={url("/static/bg-squared-400.png")}
          />
          <link rel="icon" type="image/png" sizes="72x72" href={url("/static/favicon-72.png")} />
        </Head>
        <body className="htmlbody">
          <link href={url("/static/fa/css/fontawesome-all.min.css")} rel="stylesheet" />
          <Main />
          <NextScript />
        </body>
      </html>
    )
  }
}
