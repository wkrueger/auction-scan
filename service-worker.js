self.addEventListener("install", event => {
  console.log("New worker version installing.")
})

self.addEventListener("activate", event => {
  console.log("sw activated.")
})

self.addEventListener("push", function(event) {
  console.log("Received push notification", event.data)

  async function process() {
    try {
      var json = event.data.json()
      self.registration.showNotification(json.notification.title, json.notification.content)
    } catch (err) {
      console.error("Failed serializing push data")
      throw err
    }
  }
  event.waitUntil(process())
})

self.addEventListener("notificationclick", function(event) {
  console.log("On notification click: ", event.notification.tag)
  event.notification.close()

  // This looks to see if the current is already open and
  // focuses if it is
  event.waitUntil(
    clients
      .matchAll({
        type: "window",
      })
      .then(function(clientList) {
        for (var i = 0; i < clientList.length; i++) {
          var client = clientList[i]
          if (client.url == "/dashboard" && "focus" in client) return client.focus()
        }
        if (clients.openWindow) return clients.openWindow("/dashboard?tab=scan")
      }),
  )
})
