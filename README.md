# ah-scanner

Website/service written with next.js

What is used/features:

- React+redux+typescript
- SSR
- < 300KB initial download (gzipped)
- Basic auth flow
- Emails through mailgun, browser notifications

**How to start**

Check the VSCode `launch.json`. Set `NODE_ENV=production` accordingly. We have 2 main scripts,
one for the website, other for the "worker".

The worker runs 1/hour updating the databases, sending notifications and et cetera.

For testing purposes, mailgun allows sending email without domain through its sandbox domain.

**Initial setup**

`nextpress` will crash the program on the first run and create an `envfile.env` file with the required
variables needed. Fill it up.

One also needs to create the database in mysql beforehand. (other dbs not yet supported by nextpress -- it would
be a matter of 3 lines o code tough).

WTFPL License.
