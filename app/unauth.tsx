import React from "react"
import { url, JsonForm, ajaxRequest } from "./common/site"
import Head from "next/head"
import Link from "next/link"

export default () => {
  return (
    <>
      <StyleTag />

      <SEOTags />

      <div className="container v-aligner">
        <div className="heading">
          <h1>ahscanner</h1>
          <h2 className="subtitle text-primary" style={{ fontSize: "1rem", fontWeight: 400 }}>
            (Scans the World of Warcraft auction house for deals.)
          </h2>
        </div>
        <div className="buttons">
          <button
            className="btn btn-primary btn-lg"
            type="button"
            style={TRY_NOW}
            onClick={doGuestLogin}
          >
            Try it out
          </button>
          <p className="text-primary">or login with an account:</p>
        </div>
        <div className="logins">
          <div>
            <div className="card login-card text-center border-dark">
              <div className="card-body">
                <h4 className="card-title">Create account</h4>
                <Form1 />
              </div>
            </div>
            <div style={{ height: "2em" }} />
          </div>
          <div>
            <div className="card login-card text-center border-dark">
              <div className="card-body">
                <h4 className="card-title">Existing user</h4>
                <Form2 />
              </div>
            </div>
            <div style={{ height: "2em", textAlign: "center" }}>
              <a href={url("/auth/request-password-reset")}>Forgot password</a>
            </div>
          </div>
        </div>
        <div className="buttons" />
        <div className="footers">
          <Link href="/privacy-policy">
            <a>Privacy policy</a>
          </Link>
          <br />
          World of Warcraft and related artwork is copyright of Blizzard Entertainment, Inc.
        </div>
      </div>
    </>
  )
}

const Form1 = () => (
  <JsonForm
    id="newUserForm"
    url={url("/user/createUser")}
    onSuccess={({ setMessage, resetForm }) => {
      setMessage("User successfully created. Check your email.")
      resetForm()
    }}
    render={info => (
      <>
        <JsonForm.AlertBox message={info.message} onHideError={info.hide} />
        <div className="form-group">
          <label htmlFor="newUserEmail">Email</label>
          <input
            type="email"
            name="newUserEmail"
            id="newUserEmail"
            className="form-control"
            required
          />
        </div>
        <div className="form-group">
          <label htmlFor="newUserPwd">Password</label>
          <input
            type="password"
            name="newUserPwd"
            id="newUserPwd"
            className="form-control"
            required
          />
        </div>
        <JsonForm.SubmitButton state={info.state} idleText="Go" />
      </>
    )}
  />
)

const Form2 = () => (
  <JsonForm
    id="existingUserForm"
    url={url("/user/login")}
    onSuccess={() => {
      //session auth relies on document cookie, which is only updated after a
      //page reload. Woulrnt work if we just redirected with next router.
      window.location.href = "/dashboard"
    }}
    render={info => {
      return (
        <>
          <JsonForm.AlertBox message={info.message} onHideError={info.hide} />
          <div className="form-group">
            <label htmlFor="existingUserEmail">Email</label>
            <input
              type="email"
              name="existingUserEmail"
              id="existingUserEmail"
              className="form-control"
              required
            />
          </div>
          <div className="form-group">
            <label htmlFor="existingUserPwd">Password</label>
            <input
              type="password"
              name="existingUserPwd"
              id="existingUserPwd"
              className="form-control"
              required
            />
          </div>
          <JsonForm.SubmitButton state={info.state} idleText="Go" />
        </>
      )
    }}
  />
)

class SEOTags extends React.PureComponent {
  SITE_TITLE = "ahscanner - World of Warcraft auction house scanner."
  SITE_DESCRIPTION = "Scans the World of Warcraft auction house for deals."

  render() {
    return (
      <Head>
        <title>{this.SITE_TITLE}</title>

        <meta name="description" content={this.SITE_DESCRIPTION} />
        <meta name="keywords" content="wow world of warcraft auction house gold" />

        <meta property="og:title" content={this.SITE_TITLE} />
        <meta property="og:type" content="website" />
        <meta property="og:url" content={url("")} />
        <meta property="og:image" content={url("/static/bg-squared-400.png")} />
        <meta property="og:description" content={this.SITE_DESCRIPTION} />
      </Head>
    )
  }
}

const StyleTag = () => (
  <style jsx global>{`
    body.htmlbody {
      background-image: url(${url("/static/wallhaven-191749.png")});
      background-position: top right;
      background-repeat: no-repeat;
      background-color: #c8c7a9;
    }

    #__next {
      min-height: 100vh;
    }

    @media (min-width: 990px) {
      #__next {
        height: 100vh;
      }
    }

    .login-card {
      margin: 15px;
    }
    @media (min-width: 450px) {
      .login-card {
        width: 350px;
      }
    }

    .login-card input {
      text-align: center;
    }

    .v-aligner {
      display: flex;
      flex-direction: column;
    }
    @media (min-width: 450px) {
      .v-aligner {
        height: 100%;
      }
    }

    .heading {
      display: flex;
      flex-direction: column;
      min-height: 220px;
      justify-content: center;
      align-items: center;
    }

    .subtitle {
      position: relative;
      top: -27px;
      font-style: italic;
      text-align: center;
    }

    .heading > h1 {
      font-family: "Caveat", cursive;
      font-size: 6rem;
    }

    @media (max-width: 450px) {
      .heading > h1 {
        font-size: 4rem;
        margin-bottom: 15px;
      }
    }

    .footers {
      text-align: center;
      font-size: 11px;
      color: var(--palette4);
    }

    .logins {
      display: flex;
      flex-wrap: wrap;
      justify-content: center;
      align-items: center;
    }

    .buttons {
      display: flex;
      flex: 1;
      flex-direction: column;
      align-items: center;
      justify-content: flex-end;
    }
  `}</style>
)

const TRY_NOW: React.CSSProperties = {
  //fontSize: "35px",
  paddingLeft: "30px",
  paddingRight: "30px",
  margin: "30px",
  minHeight: "47px",
}

async function doGuestLogin() {
  await ajaxRequest({
    method: "POST",
    url: url("/user/login"),
    body: {
      existingUserEmail: "guest@guest.com",
      existingUserPwd: "guesthasnopAssword",
    },
  })
  window.location.href = "/dashboard"
}
