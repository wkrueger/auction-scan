import React from "react"
import classnames from "classnames"

export function formData(form: HTMLFormElement) {
  let out = {} as { [k: string]: string }
  for (let x = 0; x < form.elements.length; x++) {
    let element = form.elements.item(x) as HTMLInputElement
    let nameAttr = element.attributes.getNamedItem("name")
    if (nameAttr) {
      out[nameAttr.value] = element.value
    }
  }
  return out
}

export function url(inp: string, query?: { [k: string]: string }) {
  if (!query) return process.env.WEBSITE_ROOT + inp
  inp += "?"
  for (let key in query) {
    inp += encodeURIComponent(key) + "=" + encodeURIComponent(query[key])
  }
  return process.env.WEBSITE_ROOT + inp
}

export function ajaxRequest<T = {}>(params: { method: string; url: string; body: any }) {
  console.log("requesting", params)
  var method = params.method
  var url = params.url
  var body = params.body
  return new Promise<T>(function(resolve, reject) {
    body = body || {}
    let req = new XMLHttpRequest()
    req.open(method || "POST", url)
    if (typeof body == "object") {
      req.setRequestHeader("content-type", "application/json")
      req.send(JSON.stringify(body))
      req.addEventListener("load", function() {
        if (String(req.status).charAt(0) === "2") {
          try {
            let parsed = JSON.parse(req.responseText)
            if (!parsed.error) return resolve(parsed)
            reject(parsed)
          } catch (e) {
            reject({ error: Error("non JSON response at ajaxRequest") })
          }
        } else {
          try {
            reject(JSON.parse(req.responseText))
          } catch (e) {
            reject({ error: Error(req.responseText) })
          }
        }
      })
    } else {
      return reject(Error("expects JSON body"))
    }
  })
}

export function arrayFrom<T = HTMLElement>(nodelist: NodeList): T[] {
  return Array.prototype.slice.call(nodelist)
}

type ProcessingStates = "idle" | "processing" | "ready"
const jsonFormInitialState = {
  message: undefined as string | undefined | null,
  state: "idle" as ProcessingStates,
}
type JsonFormState = typeof jsonFormInitialState
type JsonFormProps = {
  url: string
  id: string
  render: (state: JsonFormState & { hide: () => void }) => JSX.Element
  onSuccess: (
    i: {
      setMessage: (msg: string) => void
      resetForm: () => void
    },
  ) => void
}
export class JsonForm extends React.Component<JsonFormProps, JsonFormState> {
  static _texts = {
    processing: "Working...",
    ready: "Success!",
  }

  state = jsonFormInitialState

  //
  static SubmitButton: React.SFC<{ state: ProcessingStates; idleText: string }> = props => {
    return (
      <button type="submit" className="btn btn-secondary">
        {props.state === "idle" ? props.idleText : JsonForm._texts[props.state]}
      </button>
    )
  }

  static AlertBox: React.SFC<{
    message: string | undefined | null
    className?: string
    onHideError?: () => void
  }> = props => {
    let alertClass = props.className || "alert-warning"
    let message = String(props.message)
    if (typeof props.message === "object" && (props.message as any).message) {
      message = (props.message as any).message
    }
    return (
      (props.message && (
        <div className={classnames("alert", alertClass)}>
          {props.onHideError && (
            <button type="button" className="close" onClick={props.onHideError}>
              <span aria-hidden="true">&times;</span>
            </button>
          )}
          <span>{message}</span>
        </div>
      )) ||
      null
    )
  }

  hide() {
    this.setState({ message: null })
  }

  async handleSubmit(e: React.FormEvent<{}>) {
    e.preventDefault()
    e.stopPropagation()
    let that = this
    const dataz = formData(e.target as HTMLFormElement)
    try {
      this.setState({ state: "processing" })
      await ajaxRequest({ method: "POST", url: this.props.url, body: dataz })
      this.setState({ state: "ready" })
      this.props.onSuccess({
        setMessage(msg: string) {
          that.setState({ message: msg })
        },
        resetForm() {
          that.formEl && that.formEl.reset()
        },
      })
    } catch (err) {
      console.log("err", err)
      this.setState({
        state: "idle",
        message: err.message ||
          (err.error && err.error.message) || { error: { message: "Error." } },
      })
    }
  }

  formEl: HTMLFormElement | undefined | null

  render() {
    const { props } = this
    let that = this
    return (
      <form id={props.id} onSubmit={this.handleSubmit.bind(this)} ref={el => (that.formEl = el)}>
        {props.render({ ...this.state, hide: this.hide.bind(this) })}
      </form>
    )
  }
}
