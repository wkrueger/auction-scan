export const ruleTypes: Record<string, string> = {
  minvalueIsBelow: "Min. value is below",
  minvalueIsOver: "Min. value is over",
  stockIsBelow: "Stock is below",
}
