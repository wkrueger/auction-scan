function urlBase64ToUint8Array(base64String: string) {
  const padding = "=".repeat((4 - (base64String.length % 4)) % 4)
  const base64 = (base64String + padding).replace(/\-/g, "+").replace(/_/g, "/")
  const rawData = window.atob(base64)
  return Uint8Array.from([...rawData].map(char => char.charCodeAt(0)))
}

export function serviceWorker() {
  let _registered: ServiceWorkerRegistration

  let nextCb = { getSubscription, unsubscribe }

  // "error eating pattern"
  // trade possible errors for nullable responses (which can be control-flow checked)
  async function init(onError?: Function) {
    if (typeof document !== "undefined" && "serviceWorker" in navigator) {
      try {
        _registered = await navigator.serviceWorker.register("./service-worker.js")
        console.log("SW registered", _registered)
        if (!_registered) return
        if (!_registered.active) return
      } catch (err) {
        console.error(err)
        onError && onError(err)
      }
      return nextCb
    }
  }

  async function getSubscription(onError?: Function) {
    try {
      let pushSubscription = await _registered.pushManager.getSubscription()
      if (!pushSubscription) {
        pushSubscription = await _registered.pushManager.subscribe({
          userVisibleOnly: true,
          applicationServerKey: urlBase64ToUint8Array(
            //FIXME hardcoded
            "BMz7ST-Ab5OKEpo59Cvm6Ugl60iefeDk-Bao7RL5bI2kIhTsM1tGop8WGdKIxh6MGcUfFLt_NpaLtABmyyy8VAE",
          ),
        })
      }
      return pushSubscription
    } catch (err) {
      console.error(err)
      onError && onError(err)
    }
  }

  async function unsubscribe() {
    try {
      let pushSubscription = await _registered.pushManager.getSubscription()
      if (!pushSubscription) return
      return pushSubscription.unsubscribe()
    } catch (err) {
      console.error("error while unsubscribing", err)
      throw err
    }
  }

  return {
    init,
  }
}
