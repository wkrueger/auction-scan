export function throttle<T>(fn: (arg: T) => void, interval: number) {
  let lastExecuted = 0
  let timeout: any = undefined
  return function(arg: T) {
    if (new Date().getTime() > lastExecuted + interval) {
      if (timeout) {
        clearTimeout(timeout)
        timeout = undefined
      }
      lastExecuted = new Date().getTime()
      return fn(arg)
    }
    //queue
    if (timeout) {
      clearTimeout(timeout)
    }
    timeout = setTimeout(() => fn(arg), lastExecuted + interval - new Date().getTime())
  }
}
