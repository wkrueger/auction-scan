import React from "react"
import * as Site from "../common/site"

import { searcherFactory } from "./searcher"

type SearchResult = { ID: number; Name: string }

export const ItemSearcher = searcherFactory<number>({
  requestText: async content => {
    const results = await Site.ajaxRequest<{ items: SearchResult[] }>({
      method: "POST",
      url: Site.url("/api/item-search"),
      body: { name: content },
    })
    return results.items
  },
  requestId: async id => {
    const results = await Site.ajaxRequest<{ items: SearchResult[] }>({
      method: "POST",
      url: Site.url("/api/item-search"),
      body: { id },
    })
    return results.items
  },
  resultLine: ({ result, idx, finishSelecting }) => (
    <div
      className="dropdown-item"
      data-index={idx}
      key={idx + "-" + result.ID}
      onClick={() => {
        finishSelecting(result)
      }}
    >
      <a href={`//www.wowhead.com/item=${result.ID}`} onClick={ev => ev.preventDefault()}>
        {result.Name} ({result.ID})
      </a>
    </div>
  ),
  onFinishSelecting() {
    $WowheadPower.hideTooltip()
  },
  textOnSelect: r => r.Name + " (" + r.ID + ")",
  popout: {
    action(value) {
      window.open(`//www.wowhead.com/item=${value}`)
    },
  },
})
