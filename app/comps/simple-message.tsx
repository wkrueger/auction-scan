import React from "react"
import { url } from "../common/site"

export default function(props: { title: string; content: string }) {
  return (
    <>
      <style global jsx>{`
        body.htmlbody {
          height: 100vh;
          background-image: url(${url("/static/wallhaven-191749.png")});
          background-position: top right;
          background-repeat: no-repeat;
          background-color: #c8c7a9;
        }

        body > div#__next {
          height: 100vh;
        }

        .v-aligner {
          height: 100%;
          display: flex;
          justify-content: center;
          align-items: center;
        }
      `}</style>

      <div className="container v-aligner">
        <div className="card text-center border-dark">
          <div className="card-body">
            <h4 className="card-title">{props.title}</h4>
            <p className="card-text">{props.content}</p>
          </div>
        </div>
      </div>
    </>
  )
}
