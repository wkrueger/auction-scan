import React from "react"
import { throttle } from "../common/throttle"
import classnames from "classnames"

export type SearchResult<T = any> = { ID: T; Name: string }
import "./searcher.css"

type Props<T> = {
  value: T | null
  onChange: (val: T | null) => void
}

const initialState = {
  inputValue: {
    text: "",
    forItem: undefined as SearchResult | undefined,
  },
  editMode: undefined as
    | undefined
    | {
        currentSelection?: number
        items: { result: SearchResult; el: JSX.Element }[]
      },
}

type Opts<T> = {
  requestText: (text: string) => Promise<SearchResult[]>
  requestId: (id: T) => Promise<SearchResult[]>
  resultLine?: ResultLine
  onFinishSelecting?: () => void
  textOnSelect?: (r: SearchResult) => string
  popout?: {
    action: (value: T) => void
  }
}

type ResultLine = React.ComponentType<{
  finishSelecting: (r: SearchResult) => void
  result: SearchResult
  idx: any
}>

const DefaultResult: ResultLine = ({ result, idx, finishSelecting }) => {
  return (
    <div
      className="dropdown-item"
      data-index={idx}
      key={idx + "-" + result.ID}
      onClick={() => {
        finishSelecting(result)
      }}
    >
      {result.Name}
    </div>
  )
}

const defaultTextOnSelect = (r: SearchResult) => r.Name

export function searcherFactory<ValueType = string>(opts: Opts<ValueType>) {
  //
  const ResultLine = opts.resultLine || DefaultResult
  const textOnSelect = opts.textOnSelect || defaultTextOnSelect
  //
  return class extends React.Component<Props<ValueType>, typeof initialState> {
    state = initialState

    __searchCache = {} as { [k: string]: SearchResult[] | undefined }

    doSearch = (ev: { target: { value: string } }) => {
      let newValue = ev.target.value
      this.setState({ inputValue: { text: newValue, forItem: undefined } })
      if (newValue.length <= 3) return
      const updated = this.requestListUpdate(newValue, this.__searchCache[newValue])
      if (updated) return
      //FIXME requestData calls requestListUpdate itself, should just return results
      //that requires a fix on throtle function, whicch cant resutn results atm
      this.requestData(newValue)
    }

    requestData = throttle(async (content: string) => {
      const results = await opts.requestText(content)
      this.requestListUpdate(content, results)
    }, 1000)

    requestListUpdate(forinput: string, updateResults?: SearchResult[]) {
      if (updateResults) {
        this.__searchCache[forinput] = updateResults
      }
      if (this.state.inputValue.text != forinput) {
        //throw away, content no longer valid
        return
      }
      const results = this.__searchCache[forinput]
      if (results === undefined) return false
      let items = results.map((result, idx) => {
        return {
          el: (
            <ResultLine
              key={idx + "--" + result.ID}
              idx={idx}
              finishSelecting={this.finishSelecting.bind(this)}
              result={result}
            />
          ),
          result,
        }
      })
      if (results.length) {
        let editMode = this.state.editMode || {}
        this.setState({ editMode: { ...editMode, items } })
      }
      //if (!results.length && this.state.editMode) this.finishSelecting(undefined)
      return true
    }

    finishSelecting(selectedResult: SearchResult | undefined) {
      opts.onFinishSelecting && opts.onFinishSelecting()
      if (selectedResult) {
        this.setState({
          editMode: undefined,
          inputValue: {
            text: textOnSelect(selectedResult),
            forItem: selectedResult,
          },
        })
        this.props.onChange(selectedResult.ID)
      } else {
        this.setState({
          editMode: undefined,
          inputValue: {
            text: "",
            forItem: undefined,
          },
        })
        this.props.onChange(null)
      }
    }

    popout = (ev: React.MouseEvent<HTMLSpanElement>) => {
      if (this.props.value) {
        opts.popout && opts.popout.action && opts.popout.action(this.props.value)
      }
      ev.preventDefault()
      ev.stopPropagation()
    }

    async componentDidMount() {
      if (this.props.value) {
        const results = await opts.requestId(this.props.value)
        this.finishSelecting(results[0])
      }
    }

    keyUp = (ev: React.KeyboardEvent<HTMLInputElement>) => {
      if (!this.state.editMode) return
      const itemsList = this.state.editMode.items
      let _currentSelection = this.state.editMode.currentSelection
      if (ev.key === "ArrowDown") {
        _currentSelection =
          _currentSelection === undefined ? 0 : (Number(_currentSelection) + 1) % itemsList.length
        this.setState({
          editMode: {
            ...this.state.editMode,
            currentSelection: _currentSelection,
          },
        })
        return
      }
      if (ev.key === "ArrowUp") {
        _currentSelection =
          _currentSelection === undefined
            ? 0
            : (Number(_currentSelection) - 1 + itemsList.length) % itemsList.length
        this.setState({
          editMode: {
            ...this.state.editMode,
            currentSelection: _currentSelection,
          },
        })
        return
      }
      if (ev.key === "Enter" && _currentSelection !== undefined) {
        itemsList[_currentSelection] && this.finishSelecting(itemsList[_currentSelection].result)
        return
      }
    }

    render() {
      return (
        <div className="dropdown item-searcher">
          <div className="input-group">
            <input
              type="text"
              name="itemDisplay"
              className={classnames("form-control", "item-display", "fill", {
                "is-valid": Boolean(this.props.value),
              })}
              value={this.state.inputValue.text}
              onChange={this.doSearch}
              onKeyUp={this.keyUp}
            />
            {opts.popout && (
              <div className="input-group-append">
                <span className="input-group-text" onClick={this.popout}>
                  <a className="tooltip-hook" href="#" target="_blank">
                    <i className="fas fa-external-link-alt" />
                  </a>
                </span>
              </div>
            )}
          </div>
          <div
            className={classnames("dropdown-menu", {
              show: Boolean(this.state.editMode),
            })}
          >
            {this.state.editMode &&
              this.state.editMode.items.map((i, idx) => {
                if (this.state.editMode && idx === this.state.editMode.currentSelection) {
                  return React.cloneElement(i.el, {
                    className: i.el.props.className + " active",
                  })
                }
                return i.el
              })}
          </div>
        </div>
      )
    }
  }
}
