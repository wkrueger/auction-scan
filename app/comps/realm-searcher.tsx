import { searcherFactory, SearchResult } from "./searcher"
import { ajaxRequest, url } from "../common/site"

export const RealmSearcher = searcherFactory<string>({
  async requestText(text) {
    const results = await ajaxRequest<{ realms: SearchResult[] }>({
      method: "POST",
      url: url("/api/realm/search"),
      body: { name: text },
    })
    return results.realms
  },
  async requestId(id) {
    const results = await ajaxRequest<{ realms: SearchResult[] }>({
      method: "POST",
      url: url("api/realm/search"),
      body: { slug: id },
    })
    return results.realms
  },
})
