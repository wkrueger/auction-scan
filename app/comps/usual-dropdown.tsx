import React from "react"
import classnames from "classnames"

type Item = { text: string; val: string }
type Props = {
  title: string
  items: Item[]
  value: string
  onChange: (s: string) => void
}

const initialState = {
  editMode: false as false | { currentSelection?: number },
}

export class UsualDropdown extends React.Component<Props, typeof initialState> {
  constructor(props: Props) {
    super(props)
    this.handleButtonClick = this.handleButtonClick.bind(this)
    this.itemSelect = this.itemSelect.bind(this)
  }

  state = initialState

  shouldComponentUpdate(nextProps: Props, nextState: typeof initialState, nextContext: any) {
    const valueChanged = this.props.value !== nextProps.value
    const listChanged = this.props.items !== nextProps.items
    const editModeChanged = this.state.editMode !== nextState.editMode
    if (valueChanged || listChanged || editModeChanged) return true
    return false
  }

  findText(value: string) {
    let found = this.props.items.filter(i => i.val === value)[0]
    return found ? found.text : "--"
  }

  itemSelect(item: Item) {
    console.log("item select", item)
    this.props.onChange(item.val)
    this.setState({ editMode: false })
  }

  handleButtonClick() {
    console.log("click")
    this.setState({ editMode: this.state.editMode ? false : {} })
  }

  keyUp = (ev: any) => {
    ev.preventDefault()
    ev.stopPropagation()
    if (!this.state.editMode) {
      if (ev.key === "ArrowDown") {
        this.setState({ editMode: { currentSelection: 0 } })
      }
      return
    }
    const itemsList = this.props.items
    let _currentSelection = this.state.editMode.currentSelection
    if (ev.key === "ArrowDown") {
      _currentSelection =
        _currentSelection === undefined ? 0 : (Number(_currentSelection) + 1) % itemsList.length
      this.setState({ editMode: { ...this.state.editMode, currentSelection: _currentSelection } })
      return
    }
    if (ev.key === "ArrowUp") {
      _currentSelection =
        _currentSelection === undefined
          ? 0
          : (Number(_currentSelection) - 1 + itemsList.length) % itemsList.length
      this.setState({ editMode: { ...this.state.editMode, currentSelection: _currentSelection } })
      return
    }
  }

  //enter has to go on keyDown bc "click" fired before keyUp
  keyDown = (ev: any) => {
    if (!this.state.editMode) return
    let _currentSelection = this.state.editMode.currentSelection
    const itemsList = this.props.items
    if (ev.key === "Enter" && _currentSelection !== undefined) {
      ev.preventDefault()
      ev.stopPropagation()
      itemsList[_currentSelection] && this.itemSelect(itemsList[_currentSelection])
      return
    }
  }

  render() {
    const props = this.props
    const root = (
      <div className="dropdown">
        <button
          className="btn dropdown-toggle btn-outline-secondary fill"
          type="button"
          data-toggle="dropdown"
          aria-expanded="false"
          onClick={this.handleButtonClick}
          onKeyUp={this.keyUp}
          onKeyDown={this.keyDown}
        >
          <span>{props.value ? this.findText(props.value) : props.title}</span>
        </button>
        <div
          className={classnames("dropdown-menu", { show: this.state.editMode !== false })}
          aria-labelledby="dropdownMenuButton"
        >
          {props.items.map((line, idx) => {
            return (
              <a
                className={classnames("dropdown-item", {
                  active: this.state.editMode && this.state.editMode.currentSelection === idx,
                })}
                href="#"
                data-value={line.val}
                key={line.val}
                onClick={() => this.itemSelect(line)}
              >
                {line.text}
              </a>
            )
          })}
        </div>
      </div>
    )

    return root
  }
}
