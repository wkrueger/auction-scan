import React from "react"
import { root, effects } from "./store/store"
import classnames from "classnames"
import { ruleTypes } from "../common/strings"

const Scan = root
  .plug()
  .mapProps(state => ({ scanTab: state.scanTab }))
  .component(props => {
    return (
      <div className="card border-dark">
        <div className="card-header">
          <h3>Scan Results</h3>
          <button
            className="btn btn-primary float-right"
            onClick={() => props.dispatch(effects.refreshScanTab())}
          >
            Refresh
          </button>
        </div>
        <div className="card-body">{props.scanTab ? <Table /> : <LoadingScanTab />}</div>
      </div>
    )
  })

const Table = root
  .plug()
  .mapProps(state => ({ scanTab: state.scanTab }))
  .component(props => {
    if (!props.scanTab) return null

    if (!props.scanTab.results.length) {
      return (
        <div style={{ textAlign: "center" }}>
          <i className="fas fa-inbox" style={{ margin: "5px" }} />
          Your rules produced no alerts! Chill time!
        </div>
      )
    }

    const initCollapsed = props.scanTab.results.length > 4

    let list = props.scanTab.results.map(result => {
      let [realmId, factionId] = result[0].split(":")
      let realmLabel =
        factionId !== "" ? `${realmId} - ${FACTION_TEXT[Number(factionId)]}` : realmId
      return (
        <Accordion
          key={result[0]}
          title={<span>{realmLabel}</span>}
          initCollapsed={initCollapsed}
          content={
            <table className="table" style={{ tableLayout: "fixed" }}>
              <thead>
                <tr>
                  <th scope="col">Rule type</th>
                  <th scope="col">Rule param.</th>
                  <th scope="col">Item</th>
                  <th scope="col">Found</th>
                </tr>
              </thead>
              <tbody>
                {result[1].map((line, idx) => {
                  return <TableLine key={line.realmName + ":" + idx} line={line} />
                })}
              </tbody>
            </table>
          }
        />
      )
    })
    return <>{list}</>
  })

const TableLine: React.SFC<{ line: T.ScanResult }> = ({ line }) => {
  if (line.type === "noDataAvailableForRealm") {
    return (
      <tr>
        <td colSpan={4} style={{ textAlign: "center" }}>
          No data available for this realm yet. (It will be probably queried within the next hours.)
        </td>
      </tr>
    )
  }
  return (
    <tr>
      <td>{ruleTypes[line.type]}</td>
      <td>{line.value}</td>
      <td>
        <a href={`//www.wowhead.com/item=${line.itemId}`} onClick={ev => ev.preventDefault()}>
          {line._itemName}
        </a>
      </td>
      <td>{(line.minimumFound && golds(line.minimumFound)) || line.stockFound || 0}</td>
    </tr>
  )
}

function golds(coppers: number) {
  return (Number(coppers) / 10000).toFixed(0) + "g"
}

const FACTION_TEXT: Record<number, string> = {
  0: "Alliance",
  1: "Horde",
}

const accordionInitialState = { collapsed: true }
class Accordion extends React.Component<
  { title: JSX.Element; content: JSX.Element; initCollapsed: boolean },
  typeof accordionInitialState
> {
  constructor(props: any) {
    super(props)
    this.state = {
      ...accordionInitialState,
      collapsed: props.initCollapsed,
    }
  }

  handleButtonClick = () => {
    this.setState({ collapsed: !this.state.collapsed })
  }

  render() {
    return (
      <div className="card">
        <div className="card-header" id="headingOne">
          <h5 className="mb-0">
            <button className="btn btn-link" type="button" onClick={this.handleButtonClick}>
              {this.props.title}
            </button>
          </h5>
        </div>

        {!this.state.collapsed && (
          <div className={classnames("collapse", { show: !this.state.collapsed })}>
            <div className="card-body">{this.props.content}</div>
          </div>
        )}
      </div>
    )
  }
}

const LoadingScanTab: React.SFC = () => {
  return (
    <div style={{ textAlign: "center" }}>
      <i className="fas fa-cog fa-spin" style={{ margin: "5px" }} />
      Waiting for dataz...
    </div>
  )
}

export default Scan
