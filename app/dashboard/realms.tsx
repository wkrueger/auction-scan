import React, { CSSProperties } from "react"
import { root, effects } from "./store/store"
import { RealmSearcher } from "../comps/realm-searcher"
import pick from "lodash/pick"

const EMPTY_STYLE: CSSProperties = {
  height: "3em",
  marginTop: "2em",
  textAlign: "center",
}

const RealmsOuter = root
  .plug()
  .mapProps(state => pick(state, ["realmList", "newRealm"]))
  .component(p => {
    if (p.realmList === undefined) {
      return <LoadingTab />
    }
    const isEmpty = p.realmList.length === 0 && !p.newRealm
    return (
      <div className="card border-dark">
        <div className="card-header">
          <h3>Realms</h3>
          <button
            className="btn btn-primary float-right"
            onClick={() => p.dispatch(root.creators.newRealm_init({}))}
          >
            Add realm
          </button>
        </div>
        <div className="card-body">
          {(isEmpty && <div style={EMPTY_STYLE}>No realms added yet!</div>) || <Table />}
        </div>
      </div>
    )
  })

const Table = root
  .plug()
  .mapProps(state => ({ realmList: state.realmList }))
  .component(p => {
    if (!p.realmList) return null
    return (
      <table className="table responsive-block">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Realm (US)</th>
            <th scope="col">Factions</th>
          </tr>
        </thead>
        <tbody className="rule-table">
          <NewRealm />
          {p.realmList.map((realm, idx) => {
            return (
              <tr key={realm.slug}>
                <td>{idx + 1}</td>
                <td>{realm.name}</td>
                <td>
                  {[realm.alliance && "Alliance", realm.horde && "Horde"].filter(e => e).join(", ")}
                  <i
                    className="delete-btn far fa-window-close"
                    onClick={() => p.dispatch(effects.removeRealm(realm.slug))}
                  />
                </td>
              </tr>
            )
          })}
        </tbody>
      </table>
    )
  })

const NewRealm = root
  .plug()
  .mapProps(state => ({ newRealm: state.newRealm }))
  .component(p => {
    if (!p.newRealm) return null

    return (
      <>
        <tr className="new-item-row" key="__newRow">
          <td />
          <td className="item-cell">
            <div className="d-md-none">Add new realm:</div>
            <RealmSearcher
              value={p.newRealm.slug || null}
              onChange={val => {
                p.dispatch(root.creators.newRealm_set({ slug: val! }))
              }}
            />
          </td>
          <td className="rule-detail-cell">
            <div className="form-check form-check-inline">
              <input
                type="checkbox"
                className="form-check-input"
                value={Number(p.newRealm.alliance)}
                onChange={ev => {
                  p.dispatch(root.creators.newRealm_set({ alliance: ev.target.checked }))
                }}
              />
              <label className="form-check-label">Alliance</label>
            </div>
            <div className="form-check form-check-inline">
              <input
                type="checkbox"
                className="form-check-input"
                value={Number(p.newRealm.horde)}
                onChange={ev => {
                  p.dispatch(root.creators.newRealm_set({ horde: ev.target.checked }))
                }}
              />
              <label className="form-check-label">Horde</label>
            </div>
          </td>
        </tr>
        <tr className="new-item-row-2" key="__newRow2">
          <td colSpan={3}>
            <button className="btn" onClick={() => p.dispatch(root.creators.newRealm_hide({}))}>
              Cancel
            </button>
            <button className="btn btn-secondary" onClick={() => p.dispatch(effects.addRealm())}>
              Add
            </button>
          </td>
        </tr>
      </>
    )
  })

export default RealmsOuter

const LoadingTab: React.SFC = () => {
  return (
    <div style={{ textAlign: "center" }}>
      <i className="fas fa-cog fa-spin" style={{ margin: "5px" }} />
      Waiting for dataz...
    </div>
  )
}
