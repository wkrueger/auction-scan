import { url } from "../../common/site"
import { getDb } from "./idb"
import { isoRequest as isoreqMod, Request } from "@proerd/nextpress-client/lib/isorequest"
import { Omit } from "redutser"
import * as yup from "yup"

type ScanApiResponse = {
  results: T.ScanResult[]
}

class _BaseApi {
  constructor(public cookie?: string) {
    this.isoRequest = isoreqMod(cookie)
  }

  isoRequest: ReturnType<typeof isoreqMod>

  request<T>(i: Request) {
    return this.isoRequest.requestJson<T>(i)
  }
}

class NonGuestApi extends _BaseApi {
  isGuest = false

  async rule_list() {
    let res = await this.isoRequest.requestJson<{ rules: any[] }>({
      url: url("/api/rule/list"),
      body: {},
    })
    let rules = res.rules || []
    return rules
  }
  async rule_create(rule: Omit<Required<T.Rule_Client>, "id">) {
    await this.isoRequest.requestJson({
      url: url("/api/rule/create"),
      method: "POST",
      body: rule,
    })
  }
  async rule_remove(id: number) {
    await this.isoRequest.requestJson({
      url: url("/api/rule/remove"),
      method: "POST",
      body: { id },
    })
  }

  async realm_list() {
    let resp = await this.isoRequest.requestJson<{ realms: T.NewRealm[] }>({
      url: url("/api/realm/list"),
      method: "POST",
      body: {},
    })
    return resp.realms
  }
  async realm_create(realm: T.NewRealm) {
    await this.isoRequest.requestJson({
      url: url("/api/realm/create"),
      method: "POST",
      body: realm,
    })
  }
  async realm_remove(slug: string) {
    await this.isoRequest.requestJson({
      url: url("/api/realm/remove"),
      method: "POST",
      body: { slug },
    })
  }

  async scan() {
    let { results } = await this.isoRequest.requestJson<ScanApiResponse>({
      url: url("/api/scan"),
      method: "POST",
      body: {},
    })
    return results
  }
}

class GuestApi extends _BaseApi implements NonGuestApi {
  isGuest = true
  idb = getDb()

  rule_list() {
    return this.idb.query("rules")
  }
  async rule_create(rule: Omit<Required<T.Rule_Client>, "id">) {
    if (!rule._itemName) {
      let resp = await this.isoRequest.requestJson<{ items: { Name: string }[] }>({
        url: url("/api/item-search"),
        body: { id: rule.itemId },
      })
      if (!resp.items.length) throw Error("Failed acquiring item.")
      rule._itemName = resp.items[0].Name
    }
    return this.idb.upsert("rules", rule)
  }
  rule_remove(id: number) {
    return this.idb.deleteObject("rules", id)
  }
  realm_list() {
    return this.idb.query("realms")
  }
  async realm_create(realm: T.NewRealm) {
    if (!realm.horde && !realm.alliance) throw Error("Select at least one faction!")
    let resp = await this.isoRequest.requestJson<{ realms: { Name: string; connected: string }[] }>(
      {
        url: url("/api/realm/search"),
        body: { slug: realm.slug },
      },
    )
    if (!resp.realms.length) throw Error("Failed checking realm.")
    realm.name = resp.realms[0].Name
    realm.connected = resp.realms[0].connected
    realmSchema.validateSync(realm as any)
    return this.idb.upsert("realms", realm)
  }
  realm_remove(slug: string) {
    return this.idb.deleteObject("realms", slug)
  }

  async scan() {
    const rules = await this.rule_list()
    const realms = await this.realm_list()
    let { results } = await this.isoRequest.requestJson<ScanApiResponse>({
      url: url("/api/guest-scan"),
      method: "POST",
      body: { rules, realms },
    })
    return results
  }
}

const realmSchema = yup
  .object({
    slug: yup.string().required(),
    name: yup.string().required(),
    connected: yup.string().required(),
    alliance: yup.boolean().required(),
    horde: yup.boolean().required(),
  })
  .noUnknown()

export function apiInit(isGuest: boolean, cookie?: string) {
  return isGuest ? new GuestApi(cookie) : new NonGuestApi(cookie)
}
