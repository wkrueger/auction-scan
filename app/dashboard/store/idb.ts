import dbproto from "dbproto"

let _dbwrap: InstanceType<typeof dbproto>

export function getDb() {
  if (_dbwrap) return _dbwrap
  const dbwrap = new dbproto("main", 2)

  dbwrap.upgradeHook = event => {
    const db: IDBDatabase = (event.target as any).result

    if (event.oldVersion < 2) {
      db.createObjectStore("rules", { keyPath: "id", autoIncrement: true })
      db.createObjectStore("realms", { keyPath: "slug", autoIncrement: true })
    }
  }
  _dbwrap = dbwrap
  return dbwrap
}

declare global {
  namespace DbProtoTypes {
    export interface Stores {
      rules: T.Rule_Client
      realms: T.NewRealm
    }
  }
}
