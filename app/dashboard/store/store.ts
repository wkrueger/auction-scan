import "../../common/sw-init"
import { createStore, applyMiddleware, compose } from "redux"
import { createRedutser } from "redutser"
import thunkMw from "redux-thunk"
import { ajaxRequest, url } from "../../common/site"
import { createEffects } from "@proerd/nextpress-client/lib/connext"
import { asyncThunkErrorMw } from "@proerd/nextpress-client/lib/error"
import { serviceWorker } from "../../common/sw-init"
import { apiInit } from "./api-wrapper"
import groupBy from "lodash/groupBy"
import toPairs from "lodash/toPairs"

const isClient = typeof navigator !== "undefined"

export async function storeInitServer(email: string, cookie: string) {
  console.log("storeinitserver")
  const store = getStore()
  let isGuest = store.getState().userIsGuest
  const api = apiInit(isGuest, cookie)
  store.dispatch(root.creators.__initServer({ email, api }))
  if (!isGuest && !isClient) {
    console.log("server load data")
    const { dispatch, getState } = store
    await Promise.all([
      effects.reloadRules()(dispatch as any, getState),
      effects.reloadRealms()(dispatch as any, getState),
    ])
  }
  return store
}

export function storeInitClient(state: typeof initialState) {
  console.log("storeinitclient")
  const store = getStore()
  const api = state.__api.request ? state.__api : apiInit(state.userIsGuest)
  store.dispatch(root.creators.__initClient({ fullState: state, api: api }))
  if (state.userIsGuest && isClient) {
    console.log("client load data")
    const { dispatch, getState } = store
    effects.reloadRules()(dispatch as any, getState)
    effects.reloadRealms()(dispatch as any, getState)
  }
  return store
}

function getStore() {
  const composeEnhancers =
    (typeof window !== "undefined" && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose
  const store = createStore(
    root.reducer,
    composeEnhancers(applyMiddleware(asyncThunkErrorMw(), thunkMw)),
  )
  return store
}

const initialState = {
  newRule: undefined as undefined | T.Rule_Client,
  ruleList: undefined as undefined | T.Rule_Client[],
  showUserDropdown: false,
  error: undefined as string | undefined,
  selectedTab: "rules" as "rules" | "realms" | "scan" | "options",
  newRealm: undefined as undefined | T.NewRealm,
  realmList: undefined as undefined | T.NewRealm[],
  scanTab: undefined as
    | undefined
    | {
        results: [string, T.ScanResult[]][]
      },
  settings: undefined as
    | undefined
    | {
        form: { optEmailNotifications: boolean; optBrowserNotifications: boolean }
        isSaving?: 1 | 2
      },
  userIsGuest: false,
  userEmail: "",
  __api: (undefined as any) as ReturnType<typeof apiInit>,
}

type SettingsApiResponse = {
  settings: { optEmailNotifications: boolean; optBrowserNotifications: boolean }
}

type State = typeof initialState

export const root = createRedutser(initialState, {
  newRule_init: state => ({
    ...state,
    newRule: { itemId: undefined, rule: undefined },
  }),
  newRule_setField: (
    state,
    action: {
      field: keyof NonNullable<State["newRule"]>
      val: string
    },
  ) => {
    return {
      ...state,
      newRule: state.newRule && { ...state.newRule, [action.field]: action.val },
    }
  },
  newRule_hide: state => ({
    ...state,
    newRule: undefined,
  }),
  toggleUserDropdown: state => ({
    ...state,
    showUserDropdown: !state.showUserDropdown,
  }),
  ruleList_replace: (state, act: { rules: T.Rule_Client[] }) => {
    return {
      ...state,
      ruleList: act.rules || [],
    }
  },
  removeRule: (state, act: { id: number }) => ({
    ...state,
    ruleList: state.ruleList && state.ruleList.filter(r => r.id !== act.id),
  }),
  setTab: (state, tab: State["selectedTab"]) => ({
    ...state,
    selectedTab: tab,
  }),
  error: (state, action: { error: any }) => {
    console.error(action.error)
    return {
      ...state,
      error: action.error,
    }
  },
  newRealm_init: state => ({
    ...state,
    newRealm: {
      name: "",
      slug: "",
      alliance: false,
      horde: false,
    },
  }),
  newRealm_set: (state, act: Partial<T.NewRealm>) => ({
    ...state,
    newRealm: state.newRealm && { ...state.newRealm, ...act },
  }),
  newRealm_hide: state => ({
    ...state,
    newRealm: undefined,
  }),
  realmList_replace: (state, act: { realms: T.NewRealm[] }) => ({
    ...state,
    realmList: act.realms || [],
  }),
  realmList_remove: (state, act: { slug: string }) => ({
    ...state,
    realmList: state.realmList && state.realmList.filter(r => r.slug !== act.slug),
  }),
  scanTab_empty: state => ({
    ...state,
    scanTab: undefined,
  }),
  scanTab_replace: (state, act: { results: Record<string, T.ScanResult[]> }) => ({
    ...state,
    scanTab: {
      results: toPairs(act.results),
    },
  }),
  settings_set: (state, act: { settings: SettingsApiResponse["settings"] }) => ({
    ...state,
    settings: {
      form: act.settings,
      isSaving: state.settings && state.settings.isSaving,
    },
  }),
  settings_isSaving: (state, act: undefined | 1 | 2) => {
    return {
      ...state,
      settings: state.settings && { ...state.settings, isSaving: act },
    }
  },
  __initServer: (state, act: { email: string; api: State["__api"] }) => ({
    ...state,
    userEmail: act.email,
    userIsGuest: act.email === "guest@guest.com",
    __api: act.api,
  }),
  __initClient(_, act: { fullState: State; api: State["__api"] }) {
    return {
      ...act.fullState,
      __api: act.api,
    }
  },
})

export const effects = createEffects(root)({
  // - rules -
  reloadRules: () => async (dispatch, state) => {
    let rules = await state().__api.rule_list()
    dispatch(root.creators.ruleList_replace({ rules }))
  },
  createRule: () => async (dispatch, getState) => {
    const Api = getState().__api
    let state = getState().newRule!
    let rule = {
      type: state.type!,
      itemId: state.itemId!,
      value: state.value!,
      _itemName: state._itemName!,
    }
    await Api.rule_create(rule)
    dispatch(root.creators.newRule_hide({}))
    await effects.reloadRules()(dispatch, getState)
  },
  removeRule: (id: number) => async (dispatch, getState) => {
    const Api = getState().__api
    await Api.rule_remove(id)
    dispatch(root.creators.removeRule({ id }))
  },
  // - realms -
  reloadRealms: () => async (dispatch, getState) => {
    const Api = getState().__api
    let realms = await Api.realm_list()
    dispatch(root.creators.realmList_replace({ realms: realms }))
  },
  addRealm: () => async (dispatch, getState) => {
    const Api = getState().__api
    let state = getState().newRealm!
    await Api.realm_create(state)
    dispatch(root.creators.newRealm_hide({}))
    await effects.reloadRealms()(dispatch, getState)
  },
  removeRealm: (slug: string) => async (dispatch, getState) => {
    const Api = getState().__api
    await Api.realm_remove(slug)
    dispatch(root.creators.realmList_remove({ slug }))
  },
  loadTab: (tab: typeof initialState["selectedTab"]) => async (dispatch, getState) => {
    dispatch(root.creators.setTab(tab))
    if (tab === "scan") {
      await effects.refreshScanTab()(dispatch, getState)
    }
    if (typeof window !== "undefined") {
      window.history.pushState({}, "", url("/dashboard", { tab }))
    }
  },
  refreshScanTab: () => async (dispatch, getState) => {
    const Api = getState().__api
    dispatch(root.creators.scanTab_empty({}))
    let results = await Api.scan()
    let grouped = groupBy(results, (item: T.ScanResult) => item.realmName + ":" + item.faction)
    dispatch(root.creators.scanTab_replace({ results: grouped }))
  },
  loadUserSettings: () => async (dispatch, getState) => {
    const Api = getState().__api
    if (Api.isGuest) return
    console.log("loadusersettings")
    if (getState().settings) return
    let { settings } = await ajaxRequest<SettingsApiResponse>({
      url: url("/api/settings/view"),
      method: "POST",
      body: {},
    })
    dispatch(root.creators.settings_set({ settings }))
    await effects.setupBrowserNotifications()(dispatch, getState)
  },
  setupBrowserNotifications: () => async (dispatch, getState) => {
    const settings = getState().settings
    if (!settings) {
      await effects.loadUserSettings()(dispatch, getState)
    }
    if (settings!.form.optBrowserNotifications && isClient) {
      let sw = await serviceWorker().init()
      if (!sw) {
        console.log("Unable to initialize service worker")
        return
      }
      let subscription = await sw.getSubscription()
      if (!subscription) {
        console.log("unable to get subscription")
        return
      }
      let _subscription = subscription.toJSON()
      await ajaxRequest({
        url: url("/api/insert-push-subscription"),
        method: "POST",
        body: {
          endpoint: _subscription.endpoint,
          auth: _subscription.keys.auth,
          p256dh: _subscription.keys.p256dh,
        },
      })
      console.log("updated push subscription for this session")
    }
  },
  setUserSettings: (opts: SettingsApiResponse["settings"]) => async (dispatch, getState) => {
    const Api = getState().__api
    if (Api.isGuest) return
    let prev = getState().settings!.form
    if (!prev) {
      await effects.loadUserSettings()(dispatch, getState)
      prev = getState().settings!.form
    }
    dispatch(root.creators.settings_isSaving(1))
    await Promise.all([
      ajaxRequest({
        url: url("/api/settings/update"),
        method: "POST",
        body: { settings: opts },
      }),
      new Promise(res => setTimeout(res, 1000)),
    ])
    dispatch(root.creators.settings_isSaving(2))
    setTimeout(() => {
      dispatch(root.creators.settings_isSaving(undefined))
    }, 3000)
    if (prev!.optBrowserNotifications && opts.optBrowserNotifications) {
      if (typeof Notification !== "undefined" && (Notification as any).permission !== "granted") {
        await Notification.requestPermission()
      }
    }
    dispatch(root.creators.settings_set({ settings: opts }))
  },
  unsubscribePush: (cb: Function) => async (_, getState) => {
    try {
      await (async () => {
        const Api = getState().__api
        if (Api.isGuest) return
        let sw = await serviceWorker().init()
        if (!sw) return
        await sw.unsubscribe()
      })()
      cb && cb()
    } catch (err) {
      cb && cb(err)
    }
  },
})
