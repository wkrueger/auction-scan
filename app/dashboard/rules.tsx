import React, { CSSProperties } from "react"
import { root, effects } from "./store/store"
import { ItemSearcher } from "../comps/item-searcher"
import { UsualDropdown } from "../comps/usual-dropdown"
import { ruleTypes } from "../common/strings"

const RuleListOuter = root
  .plug()
  .ownProps()
  .mapProps(({ error, ruleList, newRule }) => ({
    error,
    ruleList,
    newRule,
  }))
  .component(p => {
    const empty = (!p.ruleList || !p.ruleList.length) && !p.newRule
    return (
      <div className="card border-dark">
        <div className="card-header">
          <h3>Rules</h3>
          <button
            className="btn btn-primary float-right"
            id="newRuleButton"
            onClick={() => p.dispatch(root.creators.newRule_init({}))}
          >
            New rule
          </button>
        </div>
        <div className="card-body">
          {!empty && (
            <table className="table responsive-block">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Item</th>
                  <th scope="col">Rule</th>
                  <th scope="col">Details</th>
                </tr>
              </thead>
              <tbody className="rule-table">
                <TableRows />
              </tbody>
            </table>
          )}
          {p.ruleList &&
            p.ruleList.length === 0 && <div style={EMPTY_STYLE}>No rules added yet!</div>}
          {p.ruleList === undefined && <LoadingTab />}
        </div>
      </div>
    )
  })

const EMPTY_STYLE: CSSProperties = {
  height: "3em",
  marginTop: "2em",
  textAlign: "center",
}

const TableRows = root
  .plug()
  .mapProps(state => ({
    newRule: state.newRule,
    ruleList: state.ruleList,
  }))
  .component(p => {
    let newRule =
      (p.newRule && (
        <>
          <tr className="new-item-row" key="__newRow">
            <td />
            <td className="item-cell">
              <div className="d-md-none">Add new item:</div>
              <ItemSearcher
                value={p.newRule.itemId || null}
                onChange={val => {
                  console.log("onChange", val)
                  p.dispatch(
                    root.creators.newRule_setField({
                      field: "itemId",
                      val: String(val || ""),
                    }),
                  )
                }}
              />
            </td>
            {p.newRule.itemId && (
              <td className="rule-cell">
                <Rules />
              </td>
            )}
            {p.newRule.type && (
              <td className="rule-detail-cell">
                <RuleDetails />
              </td>
            )}
          </tr>
          <tr className="new-item-row-2" key="__newRow2">
            <td colSpan={4}>
              <button className="btn" onClick={() => p.dispatch(root.creators.newRule_hide({}))}>
                Cancel
              </button>
              {p.newRule.value && (
                <button
                  className="btn btn-secondary"
                  onClick={() => p.dispatch(effects.createRule())}
                >
                  Add
                </button>
              )}
            </td>
          </tr>
        </>
      )) ||
      null

    const list = (
      <>
        {(p.ruleList || []).map((rule, idx) => {
          return (
            <tr key={rule.id}>
              <td>{idx + 1}</td>
              <td>
                {rule._itemName} ({rule.itemId})
              </td>
              <td>{ruleTypes[rule.type!]}</td>
              <td>
                {rule.value}
                <i
                  className="delete-btn far fa-window-close"
                  onClick={() => p.dispatch(effects.removeRule(rule.id!))}
                />
              </td>
            </tr>
          )
        })}
      </>
    )

    return (
      <>
        {newRule}
        {list}
      </>
    )
  })

const Rules = root
  .plug()
  .mapProps(({ newRule }) => ({ newRule }))
  .component(props => {
    if (!props.newRule) return null
    if (!props.newRule.itemId) return null
    return (
      <UsualDropdown
        items={[
          { val: "minvalueIsBelow", text: "Min. value is below" },
          { val: "minvalueIsOver", text: "Min. value is over" },
          { val: "stockIsBelow", text: "Stock is below" },
        ]}
        onChange={val => {
          props.dispatch(
            root.creators.newRule_setField({
              field: "type",
              val,
            }),
          )
        }}
        title="Rule"
        value={props.newRule.type || ""}
      />
    )
  })

const RuleDetails = root
  .plug()
  .mapProps(({ newRule }) => ({ newRule }))
  .component(props => {
    if (!props.newRule) return null
    if (!props.newRule.type) return null
    const merge = {
      value: props.newRule.value || "",
      onChange: (ev: any) => {
        props.dispatch(
          root.creators.newRule_setField({
            field: "value",
            val: ev.target.value,
          }),
        )
      },
    }
    const contents: any = {
      minvalueIsBelow: (
        <div className="input-group ruledetail">
          <label className="my-1 mr-2">Min.value</label>
          <input type="text" name="value" className="form-control small-input" {...merge} />
        </div>
      ),
      minvalueIsOver: (
        <div className="input-group ruledetail">
          <label className="my-1 mr-2">Min.value</label>
          <input type="text" name="value" className="form-control small-input" {...merge} />
        </div>
      ),
      stockIsBelow: (
        <div className="input-group ruledetail">
          <label className="my-1 mr-2">Min.value</label>
          <input type="text" name="value" className="form-control small-input" {...merge} />
        </div>
      ),
    }

    return contents[props.newRule.type] || <div />
  })

export default RuleListOuter

const LoadingTab: React.SFC = () => {
  return (
    <div style={{ textAlign: "center" }}>
      <i className="fas fa-cog fa-spin" style={{ margin: "5px" }} />
      Waiting for dataz...
    </div>
  )
}
