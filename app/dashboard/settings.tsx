import React, { CSSProperties } from "react"
import { root, effects } from "./store/store"
import classnames from "classnames"

const FORMSTYLE: CSSProperties = {
  maxWidth: "400px",
  marginLeft: "auto",
  marginRight: "auto",
}

const TITLESTYLE: CSSProperties = {
  textAlign: "center",
  marginBottom: "15px",
  padding: "5px",
  borderBottom: "solid 1px #8c8b76",
}

class OptionsInit extends React.Component<{ dispatch: any }> {
  componentDidMount() {
    this.props.dispatch(effects.loadUserSettings())
  }

  render() {
    return <OptionsInner />
  }
}

const Options = root
  .plug()
  .mapProps(() => ({}))
  .component(OptionsInit as any)

const OptionsInner = root
  .plug()
  .mapProps(({ settings, userIsGuest }) => ({ settings, userIsGuest }))
  .component(props => {
    if (props.userIsGuest) return <GuestOptions />
    if (!props.settings) return <span>Loading settings...</span>
    return (
      <div className="card border-dark">
        <style jsx global>{`
          .card-body .form-check > input {
            position: relative;
            top: 10px;
          }
        `}</style>
        <div className="card-header">
          <h3>Settings</h3>
        </div>
        <div className="card-body">
          <form
            id="settingsForm"
            style={FORMSTYLE}
            onSubmit={ev => {
              ev.preventDefault()
              ev.stopPropagation()
            }}
          >
            <h5 style={TITLESTYLE}>Notifications</h5>
            <div className="form-group row">
              <label className="col-sm-8 col-form-label text-right">Send email notifications</label>
              <div className="col-sm-4">
                <div className="form-check">
                  <input
                    className="form-check-input"
                    type="checkbox"
                    name="opt_email"
                    defaultChecked={props.settings.form.optEmailNotifications}
                  />
                </div>
              </div>
            </div>
            <div className="form-group row">
              <label className="col-sm-8 col-form-label text-right">
                Send browser notifications
              </label>
              <div className="col-sm-4">
                <div className="form-check">
                  <input
                    className="form-check-input"
                    type="checkbox"
                    name="opt_browser"
                    defaultChecked={props.settings.form.optBrowserNotifications}
                  />
                </div>
              </div>
            </div>
            <div className="text-right">
              <button
                type="submit"
                className={classnames("btn", "mb-2", { "btn-primary": !props.settings.isSaving })}
                onClick={event => {
                  event.preventDefault()
                  event.stopPropagation()
                  let form = (document.getElementById("settingsForm") as any) as Record<
                    string,
                    HTMLInputElement
                  >
                  let settings = {
                    optEmailNotifications: Boolean(form.opt_email.checked),
                    optBrowserNotifications: Boolean(form.opt_browser.checked),
                  }
                  props.dispatch(effects.setUserSettings(settings))
                }}
              >
                {props.settings.isSaving === 1
                  ? "Saving"
                  : props.settings.isSaving === 2
                    ? "Saved"
                    : "Save"}
              </button>
            </div>
          </form>
        </div>
      </div>
    )
  })

const GuestOptions: React.SFC<{}> = () => {
  return (
    <div className="card border-dark">
      <style jsx global>{`
        .card-body.guest-alert {
          display: flex;
          justify-content: center;
        }
        .card-body.guest-alert > p {
          text-align: center;
          max-width: 500px;
          font-style: italic;
        }
      `}</style>
      <div className="card-header">
        <h3>Settings</h3>
      </div>
      <div className="card-body guest-alert">
        <p>
          There are no settings available as a Guest User.<br />
          Create an account to persist your rules between devices and allow receiving deals through
          email or browser notifications.
        </p>
      </div>
    </div>
  )
}

export default Options
