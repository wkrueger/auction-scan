import classNames from "classnames"
import Head from "next/head"
import Router from "next/router"
import React from "react"
import { Store } from "redux"
import { ajaxRequest, JsonForm, url } from "../common/site"
import "./dashboard.scss"
import { effects, root, storeInitClient, storeInitServer } from "./store/store"
//comps
import Options from "./settings"
import Realms from "./realms"
import RuleList from "./rules"
import Scan from "./scan"
import { Provider } from "react-redux"

/*
import _dynamicHoc from "next/dynamic"
const dynamicHoc = _dynamicHoc as <T extends { default: any }>(module: Promise<T>) => T["default"]
const Options = dynamicHoc(import("./settings"))
const Realms = dynamicHoc(import("./realms"))
const RuleList = dynamicHoc(import("./rules"))
const Scan = dynamicHoc(import("./scan"))
*/

const Outer = root
  .plug()
  .mapProps(({ showUserDropdown, error, selectedTab, userIsGuest, userEmail }) => ({
    showUserDropdown,
    error,
    selectedTab,
    userIsGuest,
    userEmail,
  }))
  .component(p => {
    const logout = async () => {
      await p.dispatch(
        effects.unsubscribePush(async () => {
          await ajaxRequest({
            method: "POST",
            url: url("/user/logout"),
            body: {},
          })
          Router.push("/")
        }),
      )
    }

    return (
      <>
        <nav>
          <div style={{ flexBasis: "15%" }} />
          <h1 className="title-font">ahscanner</h1>
          <div className="nav-right">
            <div className="dropdown">
              <button
                className="btn btn-secondary dropdown-toggle"
                type="button"
                id="dropdownMenuButton"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
                onClick={() => p.dispatch(root.creators.toggleUserDropdown({}))}
              >
                <span className="d-none d-sm-inline">
                  {p.userIsGuest ? "Guest user" : p.userEmail}
                </span>
                <span className="d-sm-none">
                  <span className="fas fa-wrench" title="icon wrench" aria-hidden="true" />
                </span>
              </button>
              {p.showUserDropdown && (
                <div
                  className="dropdown-menu dropdown-menu-right"
                  aria-labelledby="dropdownMenuButton"
                  style={{ display: "block" }}
                >
                  <h6 className="dropdown-header d-sm-none">{p.userEmail}</h6>
                  <a className="dropdown-item" href="#" onClick={logout}>
                    Logout
                  </a>
                </div>
              )}
            </div>
          </div>
        </nav>

        <div id="content" className="container">
          <JsonForm.AlertBox
            message={p.error}
            onHideError={() => p.dispatch(root.creators.error({ error: undefined }))}
          />
          <ul className="nav justify-content-center nav-pills" style={{ marginBottom: "15px" }}>
            <li className="nav-item">
              <a
                className={classNames("nav-link", {
                  active: p.selectedTab === "rules",
                })}
                href="#"
                onClick={() => p.dispatch(effects.loadTab("rules"))}
              >
                Rules
              </a>
            </li>
            <li className="nav-item">
              <a
                className={classNames("nav-link", {
                  active: p.selectedTab === "realms",
                })}
                href="#"
                onClick={() => p.dispatch(effects.loadTab("realms"))}
              >
                Realms
              </a>
            </li>
            <li className="nav-item">
              <a
                className={classNames("nav-link", { active: p.selectedTab === "scan" })}
                href="#"
                onClick={() => p.dispatch(effects.loadTab("scan"))}
              >
                Scan
              </a>
            </li>
            <li className="nav-item">
              <a
                className={classNames("nav-link", { active: p.selectedTab === "options" })}
                href="#"
                onClick={() => p.dispatch(effects.loadTab("options"))}
              >
                Settings
              </a>
            </li>
          </ul>

          <SelectedTab tab={p.selectedTab} />
        </div>
      </>
    )
  })

const TABS = {
  rules: () => <RuleList />,
  realms: () => <Realms />,
  scan: () => <Scan />,
  options: () => <Options />,
}

const SelectedTab: React.SFC<{ tab: "rules" | "realms" | "scan" | "options" }> = props => {
  if (TABS[props.tab]) return TABS[props.tab]()
  return <RuleList />
}

type RootProps = {
  storeState: typeof root.initialState
  //tab?: string
}

class Root extends React.Component<RootProps> {
  static async getInitialProps(ctx: any) {
    const store = await storeInitServer(ctx.query.user.email, ctx.req.headers.cookie)
    const tab = ctx.req.query.tab
    if (tab && (TABS as any)[tab]) {
      await effects.loadTab(tab as any)(store.dispatch as any, store.getState)
    }
    return { storeState: store.getState() }
  }

  store: Store<typeof root.initialState, typeof root.actionTypes>

  constructor(props: RootProps) {
    super(props)
    this.store = storeInitClient(props.storeState)
  }

  async componentDidMount() {
    this.store.dispatch(effects.loadUserSettings() as any)
  }

  render() {
    return (
      <>
        <Head>
          <title>ah.scanner</title>
          <meta name="robots" content="noindex, nofollow" />
        </Head>
        <script src="//wow.zamimg.com/widgets/power.js" />
        <style jsx global>{`
          body.htmlbody {
            background-image: url(${url("/static/wallhaven-191749.png")});
          }
        `}</style>
        <Provider store={this.store}>
          <Outer />
        </Provider>
      </>
    )
  }
}

if (typeof window !== "undefined") {
  window.whTooltips = {
    colorLinks: false,
    iconizeLinks: true,
    renameLinks: true,
  }
}

export default Root
