This policy applies to all information collected or submitted on Ahscanner’s website and any other devices and platforms.

## The Short Version

Ahscanner collects as little information as possible, uses that information to provide the services of the site, and only shares that information with third parties that assist in performing those services (e.g. sending notification emails that you request). We do not sell your information to third parties for commercial purposes (e.g. selling lists of email addresses).

## Information We Collect

To create an account on Ahscanner, you must provide the following information:

- Email address
- Username
- Password

Email addresses are used for logging in to the website, resetting your password, sending notifications that you request, and customer support services.

In order to get the basic service functionality (which is: providing information compiled from the in-game auction database and sending notifications if the user opts in) we store the filters provided by the you.

Ahscanner uses cookies on the website to keep you logged in.

## Security

Ahscanner uses a variety of techniques and technologies to take reasonable steps to keep your information secure. All website access must occur using HTTPS, passwords are hashed using industry standard methods, and encryption is used for much of the data at rest.

## Accessing, Changing, or Deleting Your Information

You can delete your account and personal data on the Ahscanner account menu.

## Information for European Union Users

In order to use our Services, Ahscanner collects, stores, and uses your information outside of the European Union (“EU”). Without your consent to process your information outside of the EU, we cannot provide you with our Services. You may withdraw consent at anytime by deleting your account in the Ahscanner account page.

We will keep your personal data as long as necessary to provide you with Services or to comply with applicable laws. You can delete your account and personal data by accessing your account on the Ahscanner account menu. You can exercise your right to restrict or object to processing by deleting your account. If you have any questions or need to make an individual request, you can contact us at privacy@ahscanner.bid.

## Children

Our Services are for users age 16 and over and we do not knowingly collect personal information from children under the age of 16. If you are a parent or guardian of a child under the age of 16 and believe he or she has disclosed personal information to us please contact us at privacy@ahscanner.bid.

## Contact Us

If you have any questions regarding this privacy policy, please contact us at privacy@ahscanner.bid.

## Privacy Policy Changes

All changes to the privacy policy will be listed here.

07/07/2018: Initial version published
